BASE=day00
CHALLENGE_BASE?=challenges
TARGET_BUNDLE=target/${BASE}.js
SRC=bin/${BASE}/solution.ts
CHALLENGE=${CHALLENGE_BASE}/${BASE}.challenge

test-all:
	deno test

clean:
	rm -rf target

test:
	deno test bin/${BASE}

target:
	mkdir -p target

${TARGET_BUNDLE}: target ${SRC}
	deno bundle ${SRC} ${TARGET_BUNDLE}

solve: ${TARGET_BUNDLE} ${CHALLENGE}
	deno run --allow-read=${CHALLENGE} ${TARGET_BUNDLE} ${CHALLENGE}
