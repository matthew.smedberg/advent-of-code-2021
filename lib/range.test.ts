import { assert, assertArrayIncludes, assertEquals } from "https://deno.land/std/testing/asserts.ts"

import { Range } from "./range.ts";
import range from "./range.ts";

function collect<T>(g: Iterable<T>) {
    const values = [];
    for (const x of g) {
        values.push(x);
    }
    return values
}

Deno.test("range should yield ints in sequence until end is reached", () => {
    const values = collect(range(5));
    assertEquals(values, [0, 1, 2, 3, 4])
});

Deno.test("range should yield ints starting from a start point if two parameters are passed", () => {
    const values = collect(range(5,8));
    assertEquals(values, [5, 6, 7])
});

Deno.test("range should accept a step size as third parameter", () => {
    let values = collect(range(3, 13, 2));
    assertEquals(values, [3, 5, 7, 9, 11]);
    values = collect(range(3, 14, 2));
    assertEquals(values, [3, 5, 7, 9, 11, 13]);
});

Deno.test("Range should accept a step size", () => {
    let values = collect(new Range(7).step(3));
    // same as range(7, undefined, 3) or range(0, 7, 3)
    assertEquals(values, [0, 3, 6]);

    values = collect(new Range(9).step(3));
    assertEquals(values, [0, 3, 6]);
});

Deno.test("range should count down if step is negative", () => {
    let values = collect(range(7, -1, -2));
    assertEquals(values, [7, 5, 3, 1]);
    values = collect(new Range(-8).step(-2));
    assertEquals(values, [0, -2, -4, -6])
});

Deno.test("range should step by a fractional amount", () => {
    let values = collect(range(0, 8, 2.5));
    assertArrayIncludes(values, [0, 2.5, 5]);
    values = collect(range(0, 9, 1/3));
    assert(values.includes(2/3));
    // floating-point "error":
    assert(!values.includes(8));
    function existsApproximate(a: number, eps: number) {
        for (const v of values) {
            if (Math.abs(a - v) < eps) {
                return true
            }
        }
        return false
    }
    assert(existsApproximate(8, 0.01))
});
