export default function zipWithIndex<T>(ts: Iterable<T>): Iterable<[number, T]> {
    return new ZipWithIndex(ts);
}

export class ZipWithIndex<T> {
    constructor (
        private readonly ts: Iterable<T>
    ) {}
    [Symbol.iterator]() {
        return new ZipIter(this.ts);
    }
}

export class ZipIter<T> {
    private readonly tsIter: Iterator<T>
    private idx = 0

    constructor (
        ts: Iterable<T>
    ) {
        this.tsIter = ts[Symbol.iterator]();
    }

    next(): IteratorResult<[number, T]> {
        const nextT = this.tsIter.next();
        if (nextT.done === false) {
            const r: IteratorYieldResult<[number, T]> = {
                done: false,
                value: [this.idx, nextT.value]
            };
            this.idx += 1;
            return r
        } else {
            return { done: true } as IteratorReturnResult<[number, T]>
        }
    }
}
