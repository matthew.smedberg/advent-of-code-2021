import { readLines } from  "https://deno.land/std/io/buffer.ts";

export async function* readInput(fileName?: string) {
    if (fileName) {
        const lineEndingPat = /\r?\n/;
        const fullText = await Deno.readTextFile(fileName);
        yield* fullText.split(lineEndingPat);
    } else {
        yield* readLines(Deno.stdin);
    }
}
