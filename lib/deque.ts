export default class Deque<T> {
    // invariant: underlying.length is always a power of 2 and at least 16
    private underlying: T[] = new Array(16);
    // Invariant: this.start is always a valid index into this.underlying
    private start = 0;
    // Invariant: 0 <= _length <= underlying.length
    private _length = 0;
    get length() {
        return this._length
    }

    pushLeft(t: T) {
        if (this.length == this.underlying.length) {
            this.grow()
        }
        this.start = this.start ? this.start - 1 : this.underlying.length - 1;
        this.underlying[this.start] = t;
        this._length += 1;
    }

    popLeft(): T | undefined {
        if (this.length) {
            const r = this.underlying[this.start];
            this.start = (this.start + 1) % this.underlying.length;
            this._length -= 1;
            if (this.length < this.underlying.length / 8) {
                this.shrink()
            }
            return r
        }
    }

    pushRight(t: T) {
        if (this.length == this.underlying.length) {
            this.grow();
        }
        this.underlying[(this.start + this.length) % this.underlying.length] = t;
        this._length += 1;
    }

    popRight(): T | undefined {
        if (this.length) {
            const r = this.underlying[(this.start + this.length - 1) % this.underlying.length];
            this._length -= 1;
            if (this.length < this.underlying.length / 8) {
                this.shrink();
            }
            return r
        }
    }

    private grow() {
        const prevLength = this.underlying.length;
        this.underlying.length *= 2;
        this.underlying.copyWithin(prevLength, 0, this.start);
    }

    private shrink() {
        if (this.underlying.length <= 16) return;
        const nextLength = this.underlying.length / 2;
        const nextStart = this.start % nextLength;
        if (this.start >= nextLength) {
            // any entries from start to the end of underlying must be shifted left
            this.underlying.copyWithin(nextStart, this.start, this.underlying.length);
        } else if (this.start + this.length >= nextLength) {
            // any entries from the new end of underlying must be shifted to 0
            this.underlying.copyWithin(0, nextLength, this.start + this.length);
        }
        this.underlying.length = nextLength;
        this.start = nextStart;
    }
}
