import Hash from './insecureHash.ts';

import { assertEquals, assertNotEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test("Hash a string", () => {
    const h = new Hash;
    h.ingest("a text string");
    const d0 = h.currentDigest();

    h.ingest("123");
    const d1 = h.currentDigest();
    assertNotEquals(d0, d1);

    const h1 = new Hash;
    h1.ingest("a text string");
    assertEquals(d0, h1.currentDigest());

    h.ingest("울란바토르의 역사는 몽골이");
    assertNotEquals(h.currentDigest(), d1);
});

Deno.test("Hash a number", () => {
    const h = new Hash;
    h.ingest(0);
    const d0 = h.currentDigest();
    assertNotEquals(d0, 0);

    h.ingest(17.1);
    const d1 = h.currentDigest();
    assertNotEquals(d0, d1);

    const h1 = new Hash;
    const h2 = new Hash;
    const x = 256;
    h1.ingest(x);
    h2.ingest(x.toExponential());
    assertNotEquals(h1.currentDigest(), h2.currentDigest());
});

Deno.test("Hash an array", () => {
    const h = new Hash;
    h.ingest([]);
    const d0 = h.currentDigest();
    assertEquals(d0, 19*11**2 - 29*11 + 31);

    h.ingest(["foo", 81]);
    assertNotEquals(h.currentDigest(), d0);
});

Deno.test("In arrays, order matters", () => {
    const xs = ["foo", 42, ["bar", -2]];
    const h = new Hash;
    h.ingest(xs);

    const h1 = new Hash;
    xs.reverse();
    h1.ingest(xs);

    assertNotEquals(h.currentDigest(), h1.currentDigest());
})

Deno.test("Unhashable types throw", () => {
    const x = {
        foo: 17
    } as unknown as string;
    const h = new Hash;
    try {
        h.ingest(x);
    } catch (msg) {
        assertEquals(msg, "Undigestible type [object]")
    }
    assertEquals(h.currentDigest(), 0);

    const xs = [x];
    try {
        h.ingest(xs);
    } catch (msg) {
        assertEquals(msg, "Undigestible type [object]");
    }
    // partially successful ingestion is not rolled back!
    assertNotEquals(h.currentDigest(), 0);
});
