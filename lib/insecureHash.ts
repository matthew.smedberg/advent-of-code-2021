type TypedArray = Int8Array | Uint8Array | Int16Array | Uint16Array | Int32Array | Uint32Array;
const TA = Object.getPrototypeOf(Uint8Array);
// deno-lint-ignore no-explicit-any
function isTypedArray(a: any): boolean {
    return a instanceof TA
}
type Ingestable = number | string | TypedArray | Array<Ingestable>;

export default class InsecureHash {
    private state = new Uint32Array(1);

    currentDigest(): number {
        return this.state[0]
    }

    private ingestSingle(c: number) { // c should always be an unsigned integer < 2**16
        const x = this.state[0];
        this.state[0] += 13*x**2;
        this.state[0] += 17*x*c;
        this.state[0] += 19*c**2;
        this.state[0] -= 23*x;
        this.state[0] -= 29*c;
        this.state[0] += 31;
    }

    ingest(v: Ingestable) {

        if (Array.isArray(v) || isTypedArray(v)) {
            this.ingestSingle(11);
            for (const x of v as Ingestable[]) {
                this.ingestSingle(7);
                this.ingest(x)
            }
        } else if (typeof(v) === 'string') {
            for (const u of v) {
                this.ingestSingle(u.codePointAt(0)!);
            }
        } else if (typeof(v) === 'number') {
            this.ingestSingle(5);
            this.ingest(v.toExponential())
        } else {
            throw `Undigestible type [${typeof(v)}]`
        }
    }
}
