import Deque from "./deque.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test("Deque operations", () => {
    const queue: Deque<string> = new Deque;
    for (let i = 0; i < 65; i++) {
        if (i % 2) {
            queue.pushLeft(i.toString(2).padStart(8, '0'));
        } else {
            queue.pushRight(i.toString(2).padStart(8, '0'));
        }
    }
    assertEquals(queue.length, 65);

    let v = queue.popRight();
    assertEquals(v, "01000000");
    assertEquals(queue.length, 64);

    v = queue.popRight();
    assertEquals(v, "00111110");
    assertEquals(queue.length, 63);

    for (let i = 0; i < 31; i++) {
        v = queue.popRight();
        assertEquals(v![7], '0');
    }

    v = queue.popRight();
    assertEquals(v, "00000001");

    v = queue.popLeft();
    assertEquals(v, "00111111");
});
