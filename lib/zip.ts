export default function zip<T, U>(ts: Iterable<T>, us: Iterable<U>): Iterable<[T, U]> {
    return new Zip(ts, us);
}

export class Zip<T, U> {
    constructor (
        private readonly ts: Iterable<T>,
        private readonly us: Iterable<U>
    ) {}
    [Symbol.iterator](): Iterator<[T,U]> {
        return new ZipIter(this.ts, this.us)
    }
}

export class ZipIter<T, U> {
    private readonly tsIter: Iterator<T>
    private readonly usIter: Iterator<U>

    constructor (
        ts: Iterable<T>,
        us: Iterable<U>
    ) {
        this.tsIter = ts[Symbol.iterator]();
        this.usIter = us[Symbol.iterator]();
    }

    next(): IteratorResult<[T,U]> {
        const nextT = this.tsIter.next();
        const nextU = this.usIter.next();
        if (nextT.done === false && nextU.done === false) {
            return { done: false, value: [nextT.value, nextU.value] };
        } else {
            return { done: true } as IteratorReturnResult<[T, U]>;
        }
    }
}
