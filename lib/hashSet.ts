type Hash = string | number;

interface Hashable {
    equals(other: Hashable): boolean
    hash(): Hash
}

export default class HashSet<T extends Hashable> {
    private hashMap: Map<Hash, Set<T>> = new Map();

    size(): number {
        let s = 0;
        for (const vs of this.hashMap.values()) {
            s += vs.size;
        }
        return s
    }

    isEmpty(): boolean {
        for (const vs of this.hashMap.values()) {
            for (const _v of vs) {
                return false
            }
        }
        return true
    }

    contains(t: T): boolean {
        const existing = this.hashMap.get(t.hash())
        if (typeof(existing) === 'undefined') {
            return false
        } else {
            for (const t0 of existing) {
                if (t.equals(t0)) {
                    return true
                }
            }
            return false
        }
    }

    insert(t: T) {
        const th = t.hash();
        const existing = this.hashMap.get(th)
        if (typeof(existing) === 'undefined') {
            const s: Set<T> = new Set();
            s.add(t);
            this.hashMap.set(th, s);
        } else {
            search: {
                for (const t0 of existing) {
                    if (t.equals(t0)) {
                        break search
                    }
                }
                existing.add(t)
            }
        }
    }

    discard(t: T) {
        const th = t.hash();
        const existing = this.hashMap.get(th);
        if (existing) {
            for (const t0 of existing) {
                if (t.equals(t0)) {
                    existing.delete(t0); break
                }
            }
        }
    }

    pop(): T | undefined {
        for (const [k, ts] of this.hashMap) {
            switch (ts.size) {
                case 0:
                    this.hashMap.delete(k);
                    continue
                case 1:
                    for (const t of ts) {
                        this.hashMap.delete(k);
                        return t
                    }
                    break // unreachable, but the linter complains
                default:
                    for (const t of ts) {
                        ts.delete(t);
                        return t
                    }
            }
        }
    }

    [Symbol.iterator](): Generator<T> {
        function* gen(vals: Iterable<Set<T>>) {
            for (const s of vals) {
                yield* s
            }
        }
        return gen(this.hashMap.values())
    }
}
