type Hash = string | number

interface Hashable {
    hash(): Hash
}

interface Equals {
    equals(other: Equals): boolean
}

export default class HashMap<K extends Hashable & Equals, V> {
    private readonly underlying: Map<Hash, Map<K, V>> = new Map();

    containsKey(k: K): boolean {
        const kh = k.hash();
        if (this.underlying.has(kh)) {
            const kvs = this.underlying.get(kh)!;
            for (const [k0, _] of kvs) {
                if (k.equals(k0)) {
                    return true
                }
            }
            return false
        } else {
            return false
        }
    }

    get(k: K): V | undefined {
        const kh = k.hash();
        if (this.underlying.has(kh)) {
            const kvs = this.underlying.get(kh)!;
            for (const [k0, v0] of kvs) {
                if (k.equals(k0)) {
                    return v0
                }
            }
        }
    }

    put(k: K, v: V) {
        const kh = k.hash();
        if (this.underlying.has(kh)) {
            const kvs = this.underlying.get(kh)!;
            for (const [k0, _] of kvs) {
                if (k.equals(k0)) {
                    kvs.delete(k0);
                    kvs.set(k, v);
                    return
                }
                kvs.set(k, v);
            }
        } else {
            const kvs = new Map<K,V>();
            kvs.set(k, v);
            this.underlying.set(kh, kvs);
        }
    }

    putOrUpdate(k: K, ifEmpty: () => V, update: (v: V) => V): V {
        const kh = k.hash();
        if (this.underlying.has(kh)) {
            const kvs = this.underlying.get(kh)!;
            for (const [k0, v0] of kvs) {
                if (k.equals(k0)) {
                    const v = update(v0);
                    kvs.set(k0, v);
                    return v
                }
            }
            const v = ifEmpty();
            kvs.set(k, v);
            return v
        } else {
            const kvs = new Map<K, V>();
            const v = ifEmpty()
            kvs.set(k, v);
            this.underlying.set(kh, kvs);
            return v
        }
    }

    delete(k: K) {
        const kvs = this.underlying.get(k.hash());
        if (kvs) {
            for (const k0 of kvs.keys()) {
                if (k.equals(k0)) {
                    kvs.delete(k0);
                }
            }

        }
    }

    [Symbol.iterator](): Generator<[K,V]> {
        function* gen(u: Map<Hash, Map<K,V>>) {
            for (const vs of u.values()) {
                yield* vs
            }
        }
        return gen(this.underlying)
    }

    keys(): Generator<K> {
        function* gen(u: Map<Hash, Map<K, V>>) {
            for (const kvs of u.values()) {
                yield* kvs.keys()
            }
        }
        return gen(this.underlying)
    }

    values(): Generator<V> {
        function* gen(u: Map<Hash, Map<K, V>>) {
            for (const kvs of u.values()) {
                yield* kvs.values()
            }
        }
        return gen(this.underlying)
    }
}