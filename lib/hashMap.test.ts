import { assert, assertEquals } from "https://deno.land/std/testing/asserts.ts";

import range from "./range.ts";
import HashMap from "./hashMap.ts";


Deno.test("HashMap should respect equals", () => {
    class Foo {
        constructor(
            readonly w: number
        ) {}
    
        equals(other: Foo) {
            return this.w === other.w
        }
    
        hash(): number {
            return this.w
        }
    }
    const foo0a = new Foo(17);
    const foo1 = new Foo(-31.3);
    const foo0b = new Foo(17);

    assert(foo0a.equals(foo0b));
    assert(!foo0a.equals(foo1));
    assert(foo0a !== foo0b);

    type Bar = { name: string, rank: string }
    const m = new HashMap<Foo, Bar>();
    m.put(foo0a, { name: "Eisenhower", rank: "general"});
    m.put(foo1, { name: "Joker", rank: "private"});

    let { name, rank } = m.get(foo0b)!
    assertEquals(name, "Eisenhower");
    assertEquals(rank, "general");

    ({ name, rank } = m.get(foo1)!);
    assertEquals(name, "Joker");
    assertEquals(rank, "private");

    assertEquals(typeof(m.get(new Foo(0))), "undefined")
});

Deno.test("It should not care if keys choose string or number hashes", () => {
    class Foo {
        constructor (
            readonly w: string | number
        ) {}

        equals(other: Foo) {
            return this.w === other.w
        }

        hash() {
            return this.w
        }
    }

    const foo0 = new Foo(17);
    const foo1 = new Foo("bar");
    const m = new Map<Foo, string>();

    m.set(foo0, "foo 0 value");
    m.set(foo1, "foo 1 value");

    assertEquals(m.get(foo0), "foo 0 value");
});

Deno.test("putOrUpdate should insert or update", () => {
    class Foo {
        constructor (readonly x: number) {}

        hash() {
            return this.x
        }

        equals(other: Foo) {
            return this.x === other.x
        }
    }

    const m = new HashMap<Foo, string>();
    
    m.put(new Foo(25), "first");
    m.put(new Foo(31), "second");

    const updater = (s: string) => `${s}|${s}`;
    m.putOrUpdate(new Foo(49), () => "third", updater);
    m.putOrUpdate(new Foo(25), () => "third", updater);

    assertEquals(m.get(new Foo(25)), "first|first");
    assertEquals(m.get(new Foo(49)), "third");
});

Deno.test("Hash collisions should be respected", () => {
    class Foo {
        constructor (
            readonly bytes: Uint8Array
        ) {}

        equals(other: Foo) {
            if (this.bytes.length === other.bytes.length) {
                for (const idx of range(this.bytes.length)) {
                    if (this.bytes[idx] !== other.bytes[idx]) {
                        return false
                    }
                }
                return true
            } else {
                return false
            }
        }

        hash() {
            if (this.bytes.length) {
                return this.bytes[0]
            } else {
                return -1
            }
        }

        static of(...xs: number[]) {
            return new Foo(Uint8Array.from(xs))
        }
    }

    const m = new HashMap<Foo, string>();
    m.put(Foo.of(), "empty");
    m.put(Foo.of(13, 15), "unlucky");
    m.put(Foo.of(13, 31), "prime");

    assert(m.containsKey(Foo.of()));
    assertEquals(m.get(Foo.of(13, 15)), "unlucky");
    assertEquals(m.get(Foo.of(13, 31)), "prime");
    assert(!m.containsKey(Foo.of(13)));
});
