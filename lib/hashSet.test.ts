import { assert, assertEquals, fail } from "https://deno.land/std/testing/asserts.ts";

import HashSet from "./hashSet.ts";

class Foo {
    constructor(
        readonly a: string,
        readonly b: string
    ) {}

    hash() {
        return this.a.slice(0, 4) + this.b.slice(0, 4)
    }

    equals(other: Foo) {
        return this.a === other.a && this.b === other.b
    }
}

Deno.test("HashSet.insert", () => {

    const foos = new HashSet<Foo>();

    for (const foo of foos) {
        fail(`foos should be empty: ${foo} present`)
    }

    const foo0 = new Foo("first", "erst");

    foos.insert(new Foo("first", "erst"));

    let size = 0;
    for (const foo of foos) {
        size += 1;
        assert(foo.equals(foo0));
        assert(foo !== foo0);
    }

    assertEquals(size, 1);

    foos.insert(new Foo("second", "segundo"));

    const as = [];
    for (const foo of foos) {
        const {a ,b} = foo;
        as.push(a);
        switch(a) {
            case "first":
                assertEquals(b, "erst");
                break;
            case "second":
                assertEquals(b, "segundo");
                break;
            default:
                fail(`Unexpected value ${a}`);
        }
    }
    assertEquals(as.length, 2);
    assert(as.includes("first"));
    assert(as.includes("second"));

    // Hash collision case
    foos.insert(new Foo("firsute", "erstwhile"));
    size = 0;
    for (const _foo of foos) {
        size += 1;
    }
    assertEquals(size, 3);
});

Deno.test("HashMap.contains", () => {
    const foos = new HashSet<Foo>();

    foos.insert(new Foo("planet", "mars"))
    assert(foos.contains(new Foo("planet","mars")));

    foos.insert(new Foo("planet", "earth"));
    assert(foos.contains(new Foo("planet", "mars")));
    assert(foos.contains(new Foo("planet", "earth")));

    foos.insert(new Foo("plan", "eart"));
    assert(foos.contains(new Foo("planet", "mars")));
    assert(foos.contains(new Foo("planet", "earth")));
    assert(foos.contains(new Foo("plan", "eart")));

    class FooChild extends Foo {
        constructor(
            name: string,
            readonly diameter: number
        ) {
            super("planet", name)
        }
    }

    assert(foos.contains(new FooChild("earth", 22)));
});
