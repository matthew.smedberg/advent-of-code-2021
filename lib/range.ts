/**
 * Generate a range of numbers.
 * 
 * When called with one argument:
 *   range(17)
 * it will yield the integers from 0 (inclusive) to 17 (exclusive).
 * 
 * When called with two arguments:
 *   range(2, 17)
 * it will yield the integers from 2 (inclusive) to 17 (exclusive).
 * 
 * When called with three arguments:
 *   range(2, 17, 4)
 * it will yield from 2 to 17, stepping by 4.
 */
export default function range(p1: number, p2?: number, step = 1): Range {
    if (typeof(p2) === 'undefined') {
        return new Range(0, p1, step);
    } else {
        return new Range(p1, p2, step);
    }
}

export class Range implements Iterable<number> {
    private readonly stepSize: number;
    private readonly end: number;
    private yieldNext: number;

    constructor(p1: number, p2?: number, step?: number) {
        this.stepSize = typeof(step) === 'undefined' ? 1 : step;
        if (typeof(p2) === 'undefined') {
            this.yieldNext = 0;
            this.end = p1;
        } else {
            this.yieldNext = p1;
            this.end = p2;
        }
    }

    next() {
        if (this.stepSize >=0 && this.yieldNext >= this.end) {
            return {done: true} as IteratorReturnResult<undefined>
        } else if (this.stepSize < 0 && this.yieldNext <= this.end) {
            return {done: true} as IteratorReturnResult<undefined>
        } else {
            const result: IteratorYieldResult<number> = {
                done: false, value: this.yieldNext
            }
            this.yieldNext += this.stepSize;
            return result
        }
    }

    step(stepSize: number) {
        return new Range(this.yieldNext, this.end, stepSize)
    }
    
    [Symbol.iterator]() { return this }
}
