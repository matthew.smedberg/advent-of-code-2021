// Return the most common character in input
// Input is either a file (provided via an arg)
// or standard input. In either case, the input is split along newlines.

import { readInput } from "../../lib/read-input.ts";

class Counter {
    private acc: Map<string, number> = new Map();

    increment(c: string) {
        const prev = this.acc.get(c) || 0;
        this.acc.set(c, prev + 1);
    }

    max(): string | undefined {
        let bestChar, bestValue;
        for (const [k,v] of this.acc) {
            if (typeof(bestValue) === "undefined" || v > bestValue) {
                bestChar = k;
                bestValue = v;
            }
        }
        return bestChar
    }
}

async function main(args: string[]) {
    const acc = new Counter();
    const whitespace = /\s/;
    for await (const line of readInput(...args)) {
        for (const c of line) {
            if (!whitespace.test(c)) {
                acc.increment(c);
            }
        }
    }
    console.log(`Most common character: ${acc.max()}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
