import { readInput } from "../../lib/read-input.ts";

export class Trie {
    left: Trie | undefined;
    private safeLeft(): Trie {
        if (!this.left) {
            this.left = new Trie;
        }
        return this.left
    }

    right: Trie | undefined;
    private safeRight(): Trie {
        if (!this.right) {
            this.right = new Trie;
        }
        return this.right
    }

    owns = 0; // how many copies of the empty string this trie owns
    size = 0;
    depth = 0;

    contains(s: string): boolean {
        if (s.length) {
            switch (s[0]) {
                case '0':
                    return !!(this.left?.contains(s.slice(1)))
                case '1':
                    return !!(this.right?.contains(s.slice(1)))
                default:
                    return false
            }
        } else {
            return this.owns > 0
        }
    }

    insert(s: string): number {
        if (s.length) {
            switch (s[0]) {
                case '0': {
                    const k = this.safeLeft().insert(s.slice(1));
                    this.size += k;
                    this.depth = 1 + Math.max(this.left!.depth, this.right?.depth || 0);
                    return k
                }
                case '1': {
                    const k = this.safeRight().insert(s.slice(1));
                    this.size += k;
                    this.depth = 1 + Math.max(this.left?.depth || 0, this.right!.depth);
                    return k
                }
                default:
                    return 0
            }
        } else {
            this.owns += 1;
            this.size += 1;
            return 1
        }
    }

    // returns the number of '1' bits minus the number of '0' bits in the requested position
    consensus(n: number): number {
        switch(n) {
            case 0:
                return (this.right?.size || 0) - (this.left?.size || 0)
            default:
                return (this.right?.consensus(n - 1) || 0) + (this.left?.consensus(n - 1) || 0)
        }
    }
}

export function gamma(t: Trie): number {
    const bits = [];
    for (let i = 0; i < t.depth; i++) {
        bits.push(t.consensus(i) >= 0 ? '1' : '0');
    }
    return parseInt(bits.join(''), 2)
}

export function epsilon(t: Trie): number {
    const bits = [];
    for (let i = 0; i < t.depth; i++) {
        bits.push(t.consensus(i) > 0 ? '0' : '1');
    }
    return parseInt(bits.join(''), 2)
}

function select(root: Trie, goRight: (left: Trie, right: Trie) => boolean): number {
    const bits = [];
    let point = root;
    while (point.left?.size || point.right?.size) {
        const { left, right } = point;
        if (left?.size && right?.size) {
            const d = goRight(left, right);
            bits.push(d ? '1' : '0');
            point = d ? right : left;
        } else if (left?.size) {
            bits.push('0');
            point = left;
        } else if (right?.size) {
            bits.push('1');
            point = right;
        } else {
            throw "Infinite loop encountered. This should be impossible."
        }
    }

    return parseInt(bits.join(''), 2)
}

export function oxygen(t: Trie): number {
    return select(t, (left, right) => right.size >= left.size)
}

export function carbonDioxide(t: Trie): number {
    return select(t, (left, right) => right.size < left.size)
}

type Challenge = Trie;

export async function parseChallenge(input: AsyncGenerator<string>): Promise<Challenge> {
    const t = new Trie;
    for await (let line of input) {
        line = line.trim();
        if (line) {
            t.insert(line);
        }
    }
    return t
}

async function main(args: string[]) {
    const challenge = await parseChallenge(readInput(...args));
    const gam = gamma(challenge);
    const eps = epsilon(challenge);
    console.log(`Gamma = ${gam}; Epsilon = ${eps}; Power consumption = ${gam * eps}`);

    const o2 = oxygen(challenge);
    const co2 = carbonDioxide(challenge);
    console.log(`O2 = ${o2}; CO2 = ${co2}; Life support rating = ${o2 * co2}`)
}

if (import.meta.main) {
    await main(Deno.args)
}