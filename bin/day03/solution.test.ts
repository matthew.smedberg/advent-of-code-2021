import * as day03 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

async function* testInput() {
    yield* [
        "00100",
        "11110",
        "10110",
        "10111",
        "10101",
        "01111",
        "00111",
        "11100",
        "10000",
        "11001",
        "00010",
        "01010",
        ""        
    ]
}
const testChallenge = await day03.parseChallenge(testInput())

Deno.test("Parse ignores empty lines", () => {
    assertEquals(testChallenge.size, 12);
    assertEquals(testChallenge.owns, 0);

    assertEquals(testChallenge.left?.right?.left?.right?.left?.owns, 1);
})

Deno.test("gamma rate", () => {
    assertEquals(day03.gamma(testChallenge), 22);
});

Deno.test("epsilon rate", () => {
    assertEquals(day03.epsilon(testChallenge), 9);
});

Deno.test("oxygen rating", () => {
    assertEquals(day03.oxygen(testChallenge), 23);
})

Deno.test("CO2 rating", () => {
    assertEquals(day03.carbonDioxide(testChallenge), 10);
});
