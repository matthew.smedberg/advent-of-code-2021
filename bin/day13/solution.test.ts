import * as day13 from "./solution.ts";

import { assert } from "https://deno.land/std/testing/asserts.ts";

const testChallenge = await day13.parseChallenge((() => {
    async function* g() {
        yield* `6,10
        0,14
        9,10
        0,3
        10,4
        4,11
        6,0
        6,12
        4,1
        0,13
        10,12
        3,4
        3,0
        8,4
        1,10
        2,14
        8,10
        9,0
        
        fold along y=7
        fold along x=5`.split('\n')
    }
    return g()
})())

Deno.test("Challenge.step", () => {
    let c = testChallenge.step();
    assert(c.xys.contains(new day13.XY(0,0)));
    assert(!c.xys.contains(new day13.XY(1, 0)));
    assert(c.xys.contains(new day13.XY(0, 1)));

    c = c.step();
    assert(c.xys.contains(new day13.XY(1, 0)));
})
