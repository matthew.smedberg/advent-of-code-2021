import HashSet from "../../lib/hashSet.ts";
import { readInput } from "../../lib/read-input.ts";

export class XY {
    constructor (
        readonly x: number,
        readonly y: number,
    ) {}

    equals(other: XY): boolean {
        return this.x === other.x && this.y === other.y
    }

    hash(): string {
        return `${this.x},${this.y}`
    }

    fold(f: Fold): XY {
        switch (f.direction) {
            case 'x':
                if (this.x < f.coordinate) {
                    return this
                } else {
                    return new XY(2*f.coordinate - this.x, this.y)
                }
            case 'y':
                if (this.y < f.coordinate) {
                    return this
                } else {
                    return new XY(this.x, 2*f.coordinate - this.y)
                }
        }
    }

    static parse(line: string): XY | undefined {
        const pat = /(\d+),(\d+)/;
        const match = pat.exec(line);
        if (match) {
            return new XY(parseInt(match[1], 10), parseInt(match[2], 10))
        }
    }
}

class Fold {
    constructor(
        readonly direction: 'x' | 'y',
        readonly coordinate: number
    ) {}

    static parse(line: string): Fold | undefined {
        const pat = /fold along (x|y)=(\d+)/
        const match = pat.exec(line);
        if (match) {
            return new Fold(match[1] as 'x' | 'y', parseInt(match[2], 10))
        }
    }
}

class Challenge {
    readonly xys: HashSet<XY> = new HashSet;

    constructor (
        readonly folds: Fold[] = []
    ) {}

    pretty(): string {
        let [xMax, yMax] = [0,0];
        for (const xy of this.xys) {
            xMax = Math.max(xy.x + 1, xMax);
            yMax = Math.max(xy.y + 1, yMax);
        }
        const lines = new Array(yMax)
        for (let rIdx = 0; rIdx < yMax; rIdx++) {
            lines[rIdx] = new Array(xMax);
            lines[rIdx].fill(' ');
        }
        for (const xy of this.xys) {
            lines[xy.y][xy.x] = '#';
        }
        return lines.map(row => row.join('')).join('\n')

    }
    step(): Challenge {
        const fold0 = this.folds[0];
        const nextChallenge = new Challenge(this.folds.slice(1));
        if (fold0) {
            for (const xy of this.xys) {
                nextChallenge.xys.insert(xy.fold(fold0));
            }
            return nextChallenge
        } else {
            return this
        }
    }
}

export async function parseChallenge(input: AsyncGenerator<string>) {
    const challenge = new Challenge;
    for await (const line of input) {
        const xy = XY.parse(line);
        if (xy) {
            challenge.xys.insert(xy);
            continue;
        }
        const fold = Fold.parse(line);
        if (fold) {
            challenge.folds.push(fold);
        }
    }
    return challenge
}

async function main(args: string[]) {
    const challenge = await parseChallenge(readInput(...args));
    let c1 = challenge.step();
    console.log(`Part 1: ${c1.xys.size()}`);
    while (c1.folds.length) {
        c1 = c1.step();
    }
    console.log(`Part 2:\n${c1.pretty()}`)
}

if (import.meta.main) {
    await main(Deno.args);
}
