import * as day14 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

const [testTemplate, testRules] = await ((async() => {
    async function* gen() {
        yield* `NNCB

        CH -> B
        HH -> N
        CB -> H
        NH -> C
        HB -> C
        HC -> B
        HN -> C
        NN -> C
        BH -> H
        NC -> B
        NB -> B
        BN -> B
        BB -> N
        BC -> B
        CC -> N
        CN -> C`.split('\n')
    }
    return await day14.parseChallenge(gen())
})())

Deno.test("step", () => {
    let polymer = testTemplate;
    polymer = day14.step(polymer, testRules);
    assertEquals(polymer, "NCNBCHB");

    polymer = day14.step(polymer, testRules);
    assertEquals(polymer, "NBCCNBBBCBHCB");

    polymer = day14.step(polymer, testRules);
    assertEquals(polymer, "NBBBCNCCNBBNBNBBCHBHHBCHB");

    polymer = day14.step(polymer, testRules);
    assertEquals(polymer, "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB");
});

Deno.test("Part 1", () => {
    let polymer = testTemplate;
    for (let i = 0; i < 10; i++) {
        polymer = day14.step(polymer, testRules);
    }
    assertEquals(day14.part1(polymer), 1588)
});

Deno.test("Bigrams", () => {
    const b0 = day14.Bigrams.from(testTemplate)
    const b0Counts = b0.charCounts()
    assertEquals(b0Counts.get('N'), 2n);

    const b1 = b0.step(testRules);
    const b1Counts = b1.charCounts();
    assertEquals(b1Counts.get('N'), 2n);
    assertEquals(b1Counts.get('C'), 2n);
    assertEquals(b1Counts.get('B'), 2n);
    assertEquals(b1Counts.get('H'), 1n);

    assertEquals(b1.strength(), 1n);
});

Deno.test("Bigrams.strength", () => {
    let bg = day14.Bigrams.from(testTemplate);
    for (let i = 0; i < 10; i++) {
        bg = bg.step(testRules);
    }
    assertEquals(bg.strength(), 1588n);
});
