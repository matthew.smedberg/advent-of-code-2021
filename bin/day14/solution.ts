import { readInput } from "../../lib/read-input.ts";

type Template = string;
type Rules = Map<string, string>;

export async function parseChallenge(input: AsyncGenerator<string>): Promise<[Template, Rules]> {
    const template: Template = (await input.next()).value.trim();
    const rules: Rules = new Map();
    const rulePat = /(\w{2})\s*->\s*(\w)/

    for await (const line of input) {
        const match = rulePat.exec(line);
        if (match) {
            rules.set(match[1], match[2]);
        }
    }

    return [template, rules]
}

export function step(template: Template, rules: Rules): Template {
    let c0: string | undefined = undefined;
    let c1: string | undefined = undefined;
    const result: string[] = [];

    for (const c of template) {
        [c0, c1] = [c1, c];
        if (c0 && c1) {
            const ins = rules.get(c0 + c1);
            if (ins) {
                result.push(ins);
            }
        }
        result.push(c);
    }
    return result.join('')
}

export function part1(template: Template) {
    const charCount: Map<string, number> = new Map();
    for (const c of template) {
        const prev = charCount.get(c) || 0; 
        charCount.set(c, 1 + prev);
    }
    const minCount = [...charCount.values()].reduce((acc, x) => Math.min(acc, x));
    const maxCount = [...charCount.values()].reduce((acc, x) => Math.max(acc, x));
    return maxCount - minCount
}

export class Bigrams {
    constructor(
        readonly first: string,
        readonly last: string,
        readonly bigramCount: Map<string, bigint>
    ) {}

    charCounts(): Map<string, bigint> {
        const doubleCounts: Map<string, bigint> = new Map([[this.first, 1n], [this.last, 1n]]);
        for (const [bigram, n] of this.bigramCount) {
            for (const c of bigram) {
                const prev = doubleCounts.get(c) || 0n;
                doubleCounts.set(c, n + prev)
            }
        }
        return new Map([...doubleCounts].map(entry => {
            const [k, v] = entry;
            return [k, v / 2n]
        }))
    }

    strength(): bigint | undefined {
        let maxCount: bigint | undefined = undefined;
        let minCount: bigint | undefined = undefined;
        for (const v of this.charCounts().values()) {
            switch (typeof(maxCount)) {
                case "undefined":
                    maxCount = v; break
                default:
                    if (v > maxCount) {
                        maxCount = v;
                    }
            }

            switch (typeof(minCount)) {
                case "undefined":
                    minCount = v; break
                default:
                    if (v < minCount) {
                        minCount = v;
                    }
            }
        }

        if (typeof(maxCount) === "undefined" || typeof(minCount) === "undefined") {
            return undefined
        } else {
            return maxCount - minCount
        }
    }

    step(rules: Rules): Bigrams {
        const bigramCount: Map<string, bigint> = new Map();
        for (const [bigram, n] of this.bigramCount) {
            if (rules.has(bigram)) {
                const ins = rules.get(bigram)!;
                const key1 = bigram.slice(0, 1) + ins;
                bigramCount.set(key1, (bigramCount.get(key1) || 0n) + n);
                const key2 = ins + bigram.slice(1);
                bigramCount.set(key2, (bigramCount.get(key2) || 0n) + n)
            } else {
                bigramCount.set(bigram, (bigramCount.get(bigram) || 0n) + n);
            }
        }

        return new Bigrams(this.first, this.last, bigramCount)
    }

    static from(template: Template) {
        let c0: string | undefined = undefined;
        let c1: string | undefined = undefined;
        const first: string = template.at(0)!;
        const last: string = template.at(-1)!;
        const counts: Map<string, bigint> = new Map();

        for (const c of template) {
            [c0, c1] = [c1, c];
            
            if (c0 && c1) {
                const n = counts.get(c0 + c1) || 0n;
                counts.set(c0 + c1, n + 1n);
            }
        }

        return new Bigrams(first, last, counts)
    }
}

async function main(args: string[]) {
    const [template, rules] = await parseChallenge(readInput(...args))
    let evolved = template;
    for (let i = 0; i < 10; i++) {
        evolved = step(evolved, rules);
    }
    const p1 = part1(evolved);
    console.log(`Part 1: ${p1}`);

    let bg = Bigrams.from(template);
    for (let i = 0; i < 40; i++) {
        bg = bg.step(rules);
    }
    const p2 = bg.strength();
    console.log(`Part 2: ${p2}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
