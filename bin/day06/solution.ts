import { readInput } from "../../lib/read-input.ts";

export class LanternSchool {
    constructor (
        readonly spawnSchedule: BigUint64Array
    ) {}

    step(): LanternSchool {
        const nextSchedule =  new BigUint64Array(9);

        nextSchedule[6] = this.spawnSchedule[0];
        nextSchedule[8] = this.spawnSchedule[0];
        for (let i = 0; i < 8; i++) {
            nextSchedule[i] += this.spawnSchedule[i + 1];
        }
        return new LanternSchool(nextSchedule)
    }
}

export async function parseLanternSchool(input: AsyncGenerator<string>) {
    for await (let line of input) {
        line = line.trim();
        if (line) { // take the first nonempty line
            const fish = line.split(",").map(j => parseInt(j, 10));
            const schedule = new BigUint64Array(9);
            for (const f of fish) {
                schedule[f] += 1n;
            }
            return new LanternSchool(schedule)        }
    }
    // If we get here, we got bad input
    throw "Unable to parse input"
}

export function lanternSim(school: LanternSchool, days: number[]): Map<number, bigint> {
    const r = new Map<number, bigint>();
    const maxDay = days.reduce((a,b) => Math.max(a,b));
    let ev = school;
    for (let i = 1; i <= maxDay; i++) {
        ev = ev.step();
        if (days.includes(i)) {
            r.set(i, ev.spawnSchedule.reduce((acc,x) => acc + x));
        }
    }
    return r
}


async function main(args: string[]) {
    const school = await parseLanternSchool(readInput(...args));
    const simmed = lanternSim(school, [80, 256]);
    
    console.log(`Part 1: ${simmed.get(80)}`);
    console.log(`Part 2: ${simmed.get(256)}`);
}

if (import.meta.main) {
    await main(Deno.args)
}
