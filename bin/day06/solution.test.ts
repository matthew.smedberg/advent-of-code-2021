import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

import * as day06 from "./solution.ts";

Deno.test("parseLanternSchool should count fish", async () => {
    async function* input() {
        yield* `3,4,3,1,2
        `.split('\n')
    }

    const lanternSchool = await day06.parseLanternSchool(input());
    const [s0, s1, s2, s3, s4, s5, s6, s7, s8] = lanternSchool.spawnSchedule;
    assertEquals(s0, 0n);
    assertEquals(s1, 1n);
    assertEquals(s2, 1n);
    assertEquals(s3, 2n);
    assertEquals(s4, 1n);
    assertEquals(s5, 0n);
    assertEquals(s6, 0n);
    assertEquals(s7, 0n);
    assertEquals(s8, 0n);
});

Deno.test("evolve should evolve a school of fish", () => {
    const school = new day06.LanternSchool(
        BigUint64Array.from([0n,1n,1n,2n,1n,0n,0n,0n,0n])
    );

    const simmed = day06.lanternSim(school, [18, 80]);
    assertEquals(simmed.get(18), 26n);
    assertEquals(simmed.get(80), 5934n);
})
