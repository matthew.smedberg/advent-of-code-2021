import { readInput } from "../../lib/read-input.ts";

export enum ParseResultType {
    OK, CORRUPTED
}

interface ParseResult {
    parseResultType: ParseResultType
    score(): bigint
}

type OpeningBracket = '(' | '[' | '{' | '<';
type ClosingBracket = ')' | ']' | '}' | '>';

class Corrupted implements ParseResult {
    readonly parseResultType = ParseResultType.CORRUPTED;

    constructor (readonly illegalBracket: ClosingBracket) {}

    score(): bigint {
        switch (this.illegalBracket) {
            case ')': return 3n
            case ']': return 57n
            case '}': return 1197n
            case '>': return 25137n
        }
    }
}

class Ok implements ParseResult {
    readonly parseResultType = ParseResultType.OK;
    readonly openingBrackets: OpeningBracket[] = [];

    consume(b: string): ParseResult {
        switch (b) {
            case '(':
            case '[':
            case '{':
            case '<':
                this.openingBrackets.push(b);
                return this
            case ')':
            case ']':
            case '}':
            // deno-lint-ignore no-fallthrough
            case '>': {
                const ob = this.openingBrackets.pop();
                switch (`${ob}${b}`) {
                    case '()':
                    case '[]':
                    case '{}':
                    case '<>':
                        return this
                    default:
                        return new Corrupted(b)
                }
            }
            default:
                return this
        }
    }

    score(): bigint {
        function reducer(acc: bigint, ob: OpeningBracket): bigint {
            const acc5 = 5n * acc;
            switch (ob) {
                case '(': return acc5 + 1n
                case '[': return acc5 + 2n
                case '{': return acc5 + 3n
                case '<': return acc5 + 4n
            }
        }
        return this.openingBrackets.reduceRight(reducer, 0n)
    }
}

export function parseLine(line: string): ParseResult {
    let parseResult: ParseResult = new Ok;
    for (const c of line.trim()) {
        parseResult = (parseResult as Ok).consume(c);
        switch (parseResult.parseResultType) {
            case ParseResultType.CORRUPTED:
                return parseResult
        }
    }
    return parseResult
}

async function main(args: string[]) {
    let part1Sum = 0n;
    const part2Scores: bigint[] = [];
    for await (const line of readInput(...args)) {
        const parseResult = parseLine(line);
        switch (parseResult.parseResultType) {
            case ParseResultType.CORRUPTED:
                part1Sum += parseResult.score(); break
            case ParseResultType.OK:
                part2Scores.push(parseResult.score());
        }
    }
    console.log(`Part 1: ${part1Sum}`);
    const part2Median = BigUint64Array.from(part2Scores).sort()[Math.floor(part2Scores.length / 2)];
    console.log(`Part 2: ${part2Median}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
