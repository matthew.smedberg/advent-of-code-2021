import * as day10 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test("Day 10: incomplete lines", () => {
    let line = "[({(<(())[]>[[{[]{<()<>>";
    let parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.OK);
    assertEquals(parseResult.score(), 288957n);

    line = "[(()[<>])]({[<{<<[]>>(";
    parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.OK);
    assertEquals(parseResult.score(), 5566n);

    line = "(((({<>}<{<{<>}{[]{[]{}";
    parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.OK);
    assertEquals(parseResult.score(), 1480781n);

    line = "{<[[]]>}<{[{[{[]{()[[[]";
    parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.OK);
    assertEquals(parseResult.score(), 995444n);

    line = "<{([{{}}[<[[[<>{}]]]>[]]";
    parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.OK);
    assertEquals(parseResult.score(), 294n);
});

Deno.test("Day 10: corrupted lines", () => {
    let line = "{([(<{}[<>[]}>{[]{[(<()>";
    let parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.CORRUPTED);
    assertEquals(parseResult.score(), 1197n);

    line = "[[<[([]))<([[{}[[()]]]";
    parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.CORRUPTED);
    assertEquals(parseResult.score(), 3n);

    line = "[{[{({}]{}}([{[{{{}}([]";
    parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.CORRUPTED);
    assertEquals(parseResult.score(), 57n);

    line = "[<(<(<(<{}))><([]([]()";
    parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.CORRUPTED);
    assertEquals(parseResult.score(), 3n);

    line = "<{([([[(<>()){}]>(<<{{";
    parseResult = day10.parseLine(line);
    assertEquals(parseResult.parseResultType, day10.ParseResultType.CORRUPTED);
    assertEquals(parseResult.score(), 25137n);
});
