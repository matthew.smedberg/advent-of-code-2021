import { readInput } from "../../lib/read-input.ts";

export enum SnailfishNumberType {
    Single, Pair
}

export interface SnailfishNumber {
    readonly type: SnailfishNumberType
    readonly depth: number
    reduced(maxDepth?: number): boolean
    step(maxDepth?: number): SnailfishNumber
    mergeLeft(v: number): SnailfishNumber
    mergeRight(v: number): SnailfishNumber
    magnitude(): number

    plus(other: SnailfishNumber): SnailfishNumber
}

export function parse(line: string): SnailfishNumber | undefined {
    // deno-lint-ignore no-explicit-any
    function convert(arr: any): SnailfishNumber | undefined {
        if (Array.isArray(arr)) {
            const [left, right] = arr.map(convert);
            if (left && right) {
                return new Pair(left, right)
            } 
        } else if (typeof(arr) == 'number') {
            return new Single(arr)
        }
    }
    try {
        return convert(JSON.parse(line))   
    } catch (error) {
        if (!(error instanceof SyntaxError)) {
            throw error
        }
    }
}

abstract class Summable implements SnailfishNumber {
    abstract type: SnailfishNumberType
    abstract depth: number
    abstract reduced(): boolean
    abstract step(): SnailfishNumber
    abstract mergeLeft(v: number): SnailfishNumber
    abstract mergeRight(v: number): SnailfishNumber
    abstract magnitude(): number
    
    plus(other: SnailfishNumber): SnailfishNumber {
        let s: SnailfishNumber = new Pair(this, other);
        while (!s.reduced()) {
            s = s.step();
        }
        return s
    }
}

export class Single extends Summable {
    readonly type = SnailfishNumberType.Single;
    readonly depth = 0;
    constructor(
        readonly value: number
    ) {
        super()
    }

    mergeLeft(v: number): SnailfishNumber {
        return new Single(v + this.value)
    }

    mergeRight(v: number): SnailfishNumber {
        return new Single(v + this.value)
    }

    step(): SnailfishNumber {
        if (this.value >= 10) {
            return new Pair(
                new Single(Math.floor(this.value / 2)),
                new Single(Math.ceil(this.value / 2))
            )
        } else {
            return this
        }
    }

    reduced(): boolean {
        return this.value < 10
    }

    magnitude(): number {
        return this.value
    }
}

export class Pair extends Summable {
    readonly type = SnailfishNumberType.Pair;
    readonly depth: number;
    constructor(
        readonly left: SnailfishNumber,
        readonly right: SnailfishNumber
    ) {
        super();
        this.depth = 1 + Math.max(left.depth, right.depth);        
    }

    reduced(maxDepth = 4): boolean {
        return this.depth <= maxDepth && this.left.reduced(maxDepth - 1) && this.right.reduced(maxDepth - 1)
    }

    step(maxDepth = 4): SnailfishNumber {
        if (this.depth > maxDepth) {
            const [_0, r, _1] = this.explode(4, undefined, undefined);
            return r
        } else if (!this.left.reduced(maxDepth - 1)) {
            return new Pair(this.left.step(maxDepth - 1), this.right)
        } else if (!this.right.reduced(maxDepth - 1)) {
            return new Pair(this.left, this.right.step(maxDepth - 1))
        } else {
            return this
        }
    }

    private explode(
        maxDepth: number,
        leftValue: SnailfishNumber | undefined,
        rightValue: SnailfishNumber | undefined
    ): [SnailfishNumber | undefined, SnailfishNumber, SnailfishNumber | undefined] {
        if (this.left.depth && this.left.depth >= maxDepth ) {
            const [nextLV, nextCV, nextRV] = (this.left as Pair).explode(maxDepth - 1, leftValue, this.right);
            return [
                nextLV,
                nextRV ? new Pair(nextCV, nextRV) : new Pair(nextCV, this.right),
                undefined
            ]
        } else if (this.right.depth && this.right.depth >= maxDepth) {
            const [nextLV, nextCV, nextRV] = (this.right as Pair).explode(maxDepth - 1, this.left, rightValue);
            return [
                undefined,
                nextLV ? new Pair(nextLV, nextCV) : new Pair(this.left, nextCV),
                nextRV
            ]
        } else if (maxDepth <= 0) {
            // then we know both this.left and this.right are Single
            return [
                leftValue?.mergeRight((this.left as Single).value),
                new Single(0),
                rightValue?.mergeLeft((this.right as Single).value)
            ]
        } else {
            throw "Cannot explode; insufficient depth"
        }

    }

    mergeLeft(v: number): SnailfishNumber {
        return new Pair(this.left.mergeLeft(v), this.right)
    }
    mergeRight(v: number): SnailfishNumber {
        return new Pair(this.left, this.right.mergeRight(v))
    }

    magnitude(): number {
        return 3*this.left.magnitude() + 2*this.right.magnitude()
    }
}

function part1(hw: SnailfishNumber[]): number {
    return hw.reduce((acc, n) => acc.plus(n)).magnitude()
}

function part2(hw: SnailfishNumber[]): number {
    return hw.reduce((acc, n0) => hw.reduce(
        (acc1, n1) => Math.max(acc1, n0.plus(n1).magnitude()), acc),
        0
    )
}

async function main(args: string[]) {
    const hw: SnailfishNumber[] = [];
    for await (const line of readInput(...args)) {
        const n = parse(line);
        if (n) {
            hw.push(n);
        }
    }

    console.log(`Part 1: ${part1(hw)}`);
    console.log(`Part 2: ${part2(hw)}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
