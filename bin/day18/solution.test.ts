import * as day18 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test("parse", () => {
    const p5 = day18.parse("[[[[[9,8],1],2],3],4]") as day18.Pair;
    assertEquals(p5.depth, 5);
    let { left, right } = p5;
    assertEquals(left.type, day18.SnailfishNumberType.Pair);
    assertEquals(right.type, day18.SnailfishNumberType.Single);
    assertEquals((right as day18.Single).value, 4);

    ({ left, right } = (left as day18.Pair));
    assertEquals(left.type, day18.SnailfishNumberType.Pair);
    assertEquals(right.type, day18.SnailfishNumberType.Single);
    assertEquals((right as day18.Single).value, 3);

    ({ left, right } = (left as day18.Pair));
    assertEquals(left.type, day18.SnailfishNumberType.Pair);
    assertEquals(right.type, day18.SnailfishNumberType.Single);
    assertEquals((right as day18.Single).value, 2);

    ({ left, right } = (left as day18.Pair));
    assertEquals(left.type, day18.SnailfishNumberType.Pair);
    assertEquals(right.type, day18.SnailfishNumberType.Single);
    assertEquals((right as day18.Single).value, 1);

    ({ left, right } = (left as day18.Pair));
    assertEquals(left.type, day18.SnailfishNumberType.Single);
    assertEquals((left as day18.Single).value, 9);
    assertEquals(right.type, day18.SnailfishNumberType.Single);
    assertEquals((right as day18.Single).value, 8);
});

Deno.test("Explode: Example 1", () => {
    const p = day18.parse("[[[[[9,8],1],2],3],4]") as day18.Pair;
    const pa = p.step() as day18.Pair;
    let { left, right } = pa;
    assertEquals(left.type, day18.SnailfishNumberType.Pair);
    assertEquals(right.type, day18.SnailfishNumberType.Single);
    assertEquals((right as day18.Single).value, 4);

    ({left, right} = (left as day18.Pair));
    assertEquals(left.type, day18.SnailfishNumberType.Pair);
    assertEquals(right.type, day18.SnailfishNumberType.Single);
    assertEquals((right as day18.Single).value, 3);

    ({left, right} = (left as day18.Pair));
    assertEquals((right as day18.Single).value, 2);

    ({left, right} = (left as day18.Pair));
    assertEquals((left as day18.Single).value, 0);
    assertEquals((right as day18.Single).value, 9);
});

Deno.test("Explode: Example 2", () => {
    const p = day18.parse("[7,[6,[5,[4,[3,2]]]]]") as day18.Pair;
    const pa = p.step() as day18.Pair;
    let {left, right} = pa;
    assertEquals((left as day18.Single).value, 7);

    ({left, right} = (right as day18.Pair));
    assertEquals((left as day18.Single).value, 6);

    ({left, right} = (right as day18.Pair));
    assertEquals((left as day18.Single).value, 5);

    ({left, right} = (right as day18.Pair));
    assertEquals((left as day18.Single).value, 7);
    assertEquals((right as day18.Single).value, 0);
});

Deno.test("Explode: Example 3", () => {
    const p = day18.parse("[[6,[5,[4,[3,2]]]],1]") as day18.SnailfishNumber;
    const pa = p.step();
    let {left, right} = (pa as day18.Pair);
    assertEquals((right as day18.Single).value, 3);

    ({left, right} = (left as day18.Pair));
    assertEquals((left as day18.Single).value, 6);

    ({left, right} = (right as day18.Pair));
    assertEquals((left as day18.Single).value, 5);

    ({left, right} = (right as day18.Pair));
    assertEquals((left as day18.Single).value, 7);
    assertEquals((right as day18.Single).value, 0);
});

Deno.test("Explode: Example 4", () => {
    const p = day18.parse("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]") as day18.SnailfishNumber;
    const pa = p.step();

    let {left, right} = (pa as day18.Pair);
    ({left, right} = (left as day18.Pair));
    assertEquals((left as day18.Single).value, 3);

    ({left, right} = (right as day18.Pair));
    assertEquals((left as day18.Single).value, 2);

    ({left, right} = (right as day18.Pair));
    assertEquals((left as day18.Single).value, 8);
    assertEquals((right as day18.Single).value, 0);

    ({right} = (pa as day18.Pair));
    ({left, right} = right as day18.Pair);
    assertEquals((left as day18.Single).value, 9);

    ({left, right} = (right as day18.Pair));
    assertEquals((left as day18.Single).value, 5);

    ({left, right} = right as day18.Pair);
    assertEquals((left as day18.Single).value, 4);

    ({left, right} = right as day18.Pair);
    assertEquals((left as day18.Single).value, 3);
    assertEquals((right as day18.Single).value, 2);
});

Deno.test("Split", () => {
    let n = new day18.Single(10);
    let {left, right} = (n.step() as day18.Pair);
    assertEquals((left as day18.Single).value, 5);
    assertEquals((right as day18.Single).value, 5);

    n = new day18.Single(11);
    ({left, right} = n.step() as day18.Pair);
    assertEquals((left as day18.Single).value, 5);
    assertEquals((right as day18.Single).value, 6);
});

Deno.test("Plus", () => {
    const a = day18.parse("[[[[4,3],4],4],[7,[[8,4],9]]]")!;
    const b = day18.parse("[1,1]")!;
    const s = a.plus(b);

    let {left, right} = s as day18.Pair;
    ({left, right} = right as day18.Pair);
    assertEquals((left as day18.Single).value, 8);
    assertEquals((right as day18.Single).value, 1);

    ({left} = s as day18.Pair);
    ({left, right} = left as day18.Pair);
    const s01 = right as day18.Pair;
    ({left, right} = left as day18.Pair);
    assertEquals((right as day18.Single).value, 4);

    ({left, right} = left as day18.Pair);
    assertEquals((left as day18.Single).value, 0);
    assertEquals((right as day18.Single).value, 7);

    ({left, right} = s01);
    const s011 = right as day18.Pair;
    ({left, right} = left as day18.Pair);
    assertEquals((left as day18.Single).value, 7);
    assertEquals((right as day18.Single).value, 8);

    ({left, right} = s011);
    assertEquals((left as day18.Single).value, 6);
    assertEquals((right as day18.Single).value, 0);
});
