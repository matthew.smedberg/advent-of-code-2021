import Deque from "../../lib/deque.ts";
import { readInput } from "../../lib/read-input.ts";
import range from "../../lib/range.ts";

export class RiskMap {
    constructor (
        readonly map: number[][]
    ) {}

    static async parse(input: AsyncGenerator<string>) {
        const map = [];
        for await (let line of input) {
            line = line.trim();
            if (line) {
                map.push(line.split('').map(c => parseInt(c, 10)).reverse());
            }
        }

        return new RiskMap(map.reverse())
    }

    minCostPath(): number {
        const queue: Deque<[number, number, number]> = new Deque;
        const pathCosts = this.map.map(xs => new Array<number>(xs.length));
        function updateCost(rIdx: number, cIdx: number, cost: number): boolean {
            const prevCost = pathCosts[rIdx][cIdx];
            if (typeof(prevCost) !== 'number' || cost < prevCost) {
                pathCosts[rIdx][cIdx] = cost;
                return true
            } else {
                return false
            }
        }
        updateCost(0, 0, 0);
        queue.pushLeft([0,0,0]);
        while (queue.length) {
            const [rIdx, cIdx, cost] = queue.popLeft()!;
            if (pathCosts[rIdx][cIdx] === cost) {
                for (const [dx, dy] of [[1,0], [0, 1], [-1, 0], [0, -1]]) {
                    const nextRowIdx = rIdx + dx;
                    const nextColIdx = cIdx + dy;
                    const nextCost = cost + this.map[rIdx][cIdx];
                    if (
                        0 <= nextRowIdx && nextRowIdx < this.map.length &&
                        0 <= nextColIdx && nextColIdx < this.map.length &&
                        updateCost(nextRowIdx, nextColIdx, nextCost)
                    ) { queue.pushRight([nextRowIdx, nextColIdx, nextCost])}
                }
            }
        }
        return pathCosts.at(-1)?.at(-1)!
    }

    tile(n: number): RiskMap {
        function wrap(x: number): number {
            return (x - 1) % 9 + 1
        }

        const template = this.map.map(row => row.slice().reverse()).reverse();
        const nextMap: number[][] = [...range(n)].flatMap(vIdx => {
            const rows: number[][] = template.map(row => {
                return [...range(n)].flatMap(hIdx => {
                    return row.map(x => wrap(x + vIdx + hIdx))
                }).reverse()
            })

            return rows
        }).reverse()

        return new RiskMap(nextMap)
    }
}

async function main(args: string[]) {
    const riskMap = await RiskMap.parse(readInput(...args));
    const p1 = riskMap.minCostPath();
    console.log(`Part 1: ${p1}`);

    const riskMap5 = riskMap.tile(5);
    const p2 = riskMap5.minCostPath();
    console.log(`Part 2: ${p2}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
