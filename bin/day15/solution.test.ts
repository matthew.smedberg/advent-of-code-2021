import * as day15 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

const sample: day15.RiskMap = await (async () => {
    async function* lines() {
        yield* `
        1163751742
        1381373672
        2136511328
        3694931569
        7463417111
        1319128137
        1359912421
        3125421639
        1293138521
        2311944581
        `.split('\n').map(s => s.trimLeft())
    }
    return await day15.RiskMap.parse(lines())
})();

Deno.test("Part 1", () => {
    assertEquals(sample.minCostPath(), 40);
});

Deno.test("Part 2", () => {
    assertEquals(sample.tile(5).minCostPath(), 315);
})
