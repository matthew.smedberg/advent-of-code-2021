import * as day12 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

async function parse(input: string) {
    async function* lines() {
        yield* input.split('\n')
    }
    return await day12.parseCaveSystem(lines())
}
const verySmallChallenge = await parse(`
start-A
start-b
A-c
A-b
b-d
A-end
b-end`);

const slightlyLargerChallenge = await parse(`
dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc
`);

const evenLargerChallenge = await parse(`
fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW`);

Deno.test("Part 1", async () => {
    let startCave = verySmallChallenge.get("start")!;
    let p1 = await startCave.search(new Map());
    assertEquals(p1, 10);

    startCave = slightlyLargerChallenge.get("start")!;
    p1 = await startCave.search(new Map());
    assertEquals(p1, 19);

    startCave = evenLargerChallenge.get("start")!;
    p1 = await startCave.search(new Map());
    assertEquals(p1, 226);
});

Deno.test("Part 2", async () => {
    let startCave = verySmallChallenge.get("start")!;
    let p1 = await startCave.search(new Map(), 1);
    assertEquals(p1, 36);

    startCave = slightlyLargerChallenge.get("start")!;
    p1 = await startCave.search(new Map(), 1);
    assertEquals(p1, 103);

    startCave = evenLargerChallenge.get("start")!;
    p1 = await startCave.search(new Map(), 1);
    assertEquals(p1, 3509);
});
