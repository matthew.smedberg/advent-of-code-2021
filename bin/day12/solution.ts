import { readInput } from "../../lib/read-input.ts";

class Cave {
    private outEdges: Cave[] = [];
    private readonly reentrant: boolean;

    constructor(
        readonly identifier: string,
    ) {
        this.reentrant = identifier === identifier.toUpperCase();
    }

    addEdge(cave: Cave) {
        this.outEdges.push(cave);
    }

    async search(path: Map<string, number>, maxReentries = 0, reentries = 0): Promise<number> {
        const isReentry = path.get(this.identifier);

        let nextReentries: number;
        if (isReentry && !this.reentrant) {
            if (reentries >= maxReentries) {
                return 0
            } else {
                nextReentries = reentries + 1;
            }
        } else {
            nextReentries = reentries;
        }
        const nextPath = new Map(path.entries());
        nextPath.set(this.identifier, 1 + (isReentry || 0))

        const promises = this.outEdges.map(e => e.search(nextPath, maxReentries, nextReentries));
        return await Promise.all(promises).then(ps => ps.reduce((acc, p) => acc + p))
    }
}

class End extends Cave {
    constructor() {
        super("end")
    }

    // deno-lint-ignore require-await
    async search() {
        return 1
    }
}

export async function parseCaveSystem(input: AsyncGenerator<string>) {
    const system: Map<string, Cave> = new Map();
    const pat = /(\w+)-(\w+)/
    for await (const line of input) {
        const m = pat.exec(line);
        if (m) {
            let [c0, c1] = m.slice(1);
            // ensure start is always first and end is always last
            if (c1 === 'start' || c0 === 'end') {
                [c0, c1] = [c1, c0];
            }
            const getOrInsert = (key: string) => {
                const cave = system.get(key);
                if (cave) {
                    return cave
                } else {
                    const cave = key == "end" ? new End() : new Cave(key);
                    system.set(key, cave);
                    return cave
                }
            }
            const cave0 = getOrInsert(c0);
            const cave1 = getOrInsert(c1);

            if (c0 === "start" || c1 === "end") {
                cave0.addEdge(cave1);
            } else {
                cave0.addEdge(cave1);
                cave1.addEdge(cave0);
            }
        }
    }
    return system
}

async function main(args: string[]) {
    const caveSystem = await parseCaveSystem(readInput(...args));
    const startCave = caveSystem.get("start")!;
    const p1 = await startCave.search(new Map());
    const p2 = await startCave.search(new Map(), 1);
    console.log(`Part 1: ${p1}`);
    console.log(`Part 2: ${p2}`);
}

if (import.meta.main) {
    await main(Deno.args)
}
