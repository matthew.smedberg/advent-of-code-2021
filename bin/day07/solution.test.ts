import * as day07 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

const challenge = [16,1,2,0,4,2,7,1,2,14];

Deno.test("Day 7: Part 1", () => {
    assertEquals(day07.totalFuel(challenge, 0, 16, Math.abs), 37);
});

Deno.test("Day 7: Part 2", () => {
    function fuel(d: number) {
        let f = 0;
        for (let i = 1; i <= Math.abs(d); i++) {
            f += i;
        }
        return f
    }
    assertEquals(day07.totalFuel(challenge, 0, 16, fuel), 168);
});
