import range from "../../lib/range.ts";
import { readInput } from "../../lib/read-input.ts";

type Challenge = number[];

async function parseChallenge(input: AsyncGenerator<string>) {
    for await (let line of input) {
        line = line.trim()
        if (line) {
            return line.split(',').map(j => parseInt(j, 10))
        }
    }
    throw "No input"
}

function bounds(challenge: Challenge): [number, number] | undefined {
    function reducer(acc: [number, number] | undefined, x: number): [number, number] {
        if (typeof(acc) === 'undefined') {
            return [x, x]
        } else {
            const [least, greatest] = acc;
            return [Math.min(least, x), Math.max(greatest, x)]
        }
    }
    return challenge.reduce(reducer, undefined)
}

function min(gen: Iterable<number>): number | undefined {
    enum StateType {
        START, CONT
    }
    const state : { stateType: StateType, value: number | undefined} = { stateType: StateType.START, value: undefined }
    for (const x of gen) {
        switch (state.stateType) {
            case StateType.START:
                state.stateType = StateType.CONT;
                state.value = x;
                break;
            case StateType.CONT:
                if (x < state.value!) {
                    state.value = x;
                }
        }
    }
    return state.value;
}

export function totalFuel(challenge: Challenge, left: number, right: number, fuel: (x: number) => number) {
    function* gen() {
        for (const p of range(left, right)) {
            yield challenge.reduce((acc, x) => acc + fuel(x - p), 0)
        }
    }
    return min(gen())
}

async function main(args: string[]) {
    const challenge = await parseChallenge(readInput(...args));
    const [left, right] = bounds(challenge)!;
    const part1 = totalFuel(challenge, left, right, (d) => Math.abs(d));
    console.log(`Part 1: ${part1}`);

    const part2 = totalFuel(challenge, left, right, (d) => {
        const da = Math.abs(d);
        return (da + 1) * da / 2;
    });
    console.log(`Part 2: ${part2}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
