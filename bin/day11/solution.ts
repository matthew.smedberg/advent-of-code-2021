import { readInput } from "../../lib/read-input.ts";
import zipWithIndex from "../../lib/zipWithIndex.ts";

class Challenge {
    readonly population: number;
    constructor (private rows: number[][]) {
        this.population = rows.reduce((acc, row) => acc + row.length, 0);
    }

    step(){
        function* queueInit(self: Challenge): Generator<[number, number]> {
            for (const [rIdx, row] of zipWithIndex(self.rows)) {
                for (const [cIdx, _x] of zipWithIndex(row)) {
                    yield [rIdx, cIdx]
                }
            }
        }
        const queue = [...queueInit(this)];
        while (queue.length) {
            const [rIdx, cIdx] = queue.pop()!;
            this.rows[rIdx][cIdx] += 1;
            if (this.rows[rIdx][cIdx] === 10) {
                // deno-lint-ignore no-inner-declarations
                function* neighbors(self: Challenge): Generator<[number, number]> {
                    for (const dr of [-1, 0, 1]) {
                        for (const dc of [-1, 0, 1]) {
                            const rIdx1 = rIdx + dr;
                            const cIdx1 = cIdx + dc;
                            if ((dr || dc) && (0 <= rIdx1) && (0 <= cIdx1) && (rIdx1 < self.rows.length) && (cIdx1 < self.rows[rIdx1].length)) {
                                yield [rIdx1, cIdx1]
                            }
                        }
                    }
                }
                queue.push(...neighbors(this));
            }
        }

        let flashCount = 0;
        for (const row of this.rows) {
            for (const [cIdx, x] of zipWithIndex(row)) {
                if (x > 9) {
                    flashCount += 1;
                    row[cIdx] = 0;
                }
            }
        }
        return flashCount
    }
}

export async function parseChallenge(input: AsyncGenerator<string>) {
    const challengeRows = [];
    for await (let line of input) {
        line = line.trim()
        if (line) {
            challengeRows.push(
                line.split('').map(j => parseInt(j, 10))
            )
        }
    }
    return new Challenge(challengeRows)
}

export function solve(challenge: Challenge): [number, number] {
    let steps = 0;
    let totalFlashes = 0;
    let totalFlashes100: number | undefined = undefined;
    let syncSteps: number | undefined = undefined;
    while (typeof(totalFlashes100) === 'undefined' || typeof(syncSteps) === 'undefined') {
        const flashes = challenge.step();
        totalFlashes += flashes;
        steps += 1;
        if (steps === 100) {
            totalFlashes100 = totalFlashes;
        }
        if (flashes === challenge.population) {
            syncSteps = steps;
        }
    }
    return [totalFlashes100, syncSteps]
}

async function main(args: string[]) {
    const challenge = await(parseChallenge(readInput(...args)));
    const [part1, part2] = solve(challenge);
    console.log(`Part 1: ${part1}`);
    console.log(`Part 2: ${part2}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
