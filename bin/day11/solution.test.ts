import * as day11 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

const testChallenge = await (async () => {
    const input = `5483143223
    2745854711
    5264556173
    6141336146
    6357385478
    4167524645
    2176841721
    6882881134
    4846848554
    5283751526
    `;
    async function* lines() {
        yield* input.split('\n')
    }
    return await day11.parseChallenge(lines())
})();

Deno.test("Day 11", () => {
    const [part1, part2] = day11.solve(testChallenge);
    assertEquals(part1, 1656);
    assertEquals(part2, 195);
});
