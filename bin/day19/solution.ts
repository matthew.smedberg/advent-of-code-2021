import zip from "../../lib/zip.ts";
import Hash from "../../lib/insecureHash.ts";
import HashMap from "../../lib/hashMap.ts";
import HashSet from "../../lib/hashSet.ts";
import {readInput} from "../../lib/read-input.ts";

class XYZ {
    private readonly _hash: number
    constructor(
        readonly x: number,
        readonly y: number,
        readonly z: number,
    ) {
        const h = new Hash;
        h.ingest([x,y,z]);
        this._hash = h.currentDigest()
    }

    equals(other: XYZ) {
        return this.x === other.x && this.y === other.y && this.z === other.z
    }

    hash() {
        return this._hash
    }

    minus(other: XYZ): XYZ {
        return new XYZ(
            this.x - other.x,
            this.y - other.y,
            this.z - other.z,
        )
    }

    taxicabDistance(other: XYZ): number {
        const d = this.minus(other);
        return [d.x, d.y, d.z].reduce((acc, w) => acc + Math.abs(w), 0)
    }

    shift(other: XYZ): XYZ {
        return new XYZ(
            this.x + other.x,
            this.y + other.y,
            this.z + other.z,
        )
    }

    reorient(s: Reorientation): XYZ {
        const thisCoords = [this.x, this.y, this.z];
        const nextCoords = [];
        for (const [idx, sgn] of zip(s.p, s.sgn)) {
            nextCoords.push(sgn * thisCoords[idx])
        }
        const [x, y, z] = nextCoords;
        return new XYZ(x,y,z)
    }

    static parse(line: string): XYZ | undefined {
        const pat = /(-?\d+),(-?\d+),(-?\d+)/
        const m = pat.exec(line);
        if (m) {
            const [x,y,z] = m.slice(1).map(g => parseInt(g, 10));
            return new XYZ(x, y, z)
        }
    }
}

class Diff {
    private readonly _hash: number;
    private readonly d: Uint32Array;

    constructor(p0: XYZ, p1: XYZ) {
        this.d = Uint32Array.from([p0.x - p1.x, p0.y - p1.y, p0.z - p1.z].map(x => Math.abs(x)));
        this.d.sort();
        const h = new Hash;
        h.ingest(this.d);
        this._hash = h.currentDigest();
    }

    equals(other: Diff): boolean {
        if (this.d.length === other.d.length) {
            for (let idx = 0; idx < this.d.length; idx++) {
                if (this.d[idx] !== other.d[idx]) {
                    return false
                }
            }
            return true
        } else {
            return false
        }
    }

    hash() {
        return this._hash
    }
}

// Must actually be a permutation of [0,1,2]
type P3 = [0|1|2, 0|1|2, 0|1|2]

function* permutations(): Generator<P3> {
    for (const p2 of [[0,1], [1,0]]) {
        for (let idx = 2; idx >= 0; idx--) {
            const y = p2.slice() as P3;
            y.splice(idx, 0, 2);
            yield y
        }
    }
}

type Parity = -1|1
type Sgn = [Parity, Parity, Parity]

function* sgns(parity: Parity): Generator<Sgn> {
    const parities: Parity[] = [-1, 1]
    for (const p0 of parities) {
        for (const p1 of parities) {
            for (const p2 of parities) {
                const p = (p0 * p1 * p2);
                if (p == parity) {
                    yield [p0, p1, p2]
                }
            }
        }
    }
}

class Reorientation {
    constructor(
        readonly p: P3,
        readonly sgn: Sgn,
    ) {}

    static *all() {
        for (const p of permutations()) {
            const lettersMoved = [0,1,2].reduce((acc, s) => acc + Number(s != p[s]), 0);
            // If the permutation moves 2 letters, it's odd
            const parity = lettersMoved == 2 ? -1 : 1;
            for (const sgn of sgns(parity)) {
                yield new Reorientation(p, sgn)
            }
        }
    }
}

class Scanner {
    constructor(
        readonly beacons: XYZ[],
        private readonly _scanners: XYZ[] | undefined = undefined
    ) {}

    get scanners(): XYZ[] {
        if (this._scanners?.length) {
            return this._scanners.slice()
        } else {
            return [new XYZ(0,0,0)]
        }
    }

    clone(): Scanner {
        return new Scanner(
            this.beacons.slice(),
            this.scanners.slice(),
        )
    }

    reorient(m: Reorientation): Scanner {
        return new Scanner(
            this.beacons.map(b => b.reorient(m)),
            this.scanners.map(s => s.reorient(m))
        )
    }

    shift(v: XYZ): Scanner {
        return new Scanner(
            this.beacons.map(b => b.shift(v)),
            this.scanners.map(s => s.shift(v))
        )
    }

    diffs(): HashMap<Diff, [number, number]> {
        const m = new HashMap<Diff, [number, number]>();

        for (let i0 = 0; i0 < this.beacons.length; i0++) {
            for (let i1 = 0; i1 < i0; i1++) {
                m.put(new Diff(this.beacons[i0], this.beacons[i1]),[i0, i1])
            }
        }

        return m
    }

    merge(other: Scanner): Scanner {
        const diffsSelf = this.diffs();
        const diffsOther = other.diffs();

        for (const [d01, idx01] of diffsSelf) {
            const [idx0, idx1] = idx01;
            let idx01Other: [number, number];
            if (!diffsOther.containsKey(d01)) {
                continue
            } else {
                idx01Other = diffsOther.get(d01)!;
            }
            // deno-lint-ignore no-inner-declarations
            function* neighbors(): Generator<[Diff, [number, number]]> {
                for (const [d, idx] of diffsSelf) {
                    if ((idx.includes(idx0) || idx.includes(idx1)) && !d.equals(d01)) {
                        yield [d, idx]
                    }
                }
            }
            for (const [d12, idx12] of neighbors()) {
                let idx12Other: [number, number];
                if (!diffsOther.containsKey(d12)) {
                    continue
                } else {
                    idx12Other = diffsOther.get(d12)!;
                }
                const i1 = next(idx01, i => idx12.includes(i));
                const i0 = next(idx01, i => !idx12.includes(i));
                const i2 = next(idx12, i => !idx01.includes(i));
                const j1 = next(idx01Other, j => idx12Other.includes(j));
                const j0 = next(idx01Other, j => !idx12Other.includes(j));
                const j2 = next(idx12Other, j => !idx01Other.includes(j));

                if (
                    new Diff(this.beacons[i0!], this.beacons[i1!]).equals(new Diff(other.beacons[j0!], other.beacons[j1!])) &&
                    new Diff(this.beacons[i1!], this.beacons[i2!]).equals(new Diff(other.beacons[j1!], other.beacons[j2!])) &&
                    new Diff(this.beacons[i2!], this.beacons[i0!]).equals(new Diff(other.beacons[j2!], other.beacons[j0!]))
                ) {
                    const displacement01 = this.beacons[i0!].minus(this.beacons[i1!]);
                    const displacement01Other = other.beacons[j0!].minus(other.beacons[j1!]);
                    const r = next(Reorientation.all(), r => displacement01Other.reorient(r).equals(displacement01))
                    const otherReoriented = other.reorient(r);
                    const otherShifted = otherReoriented.shift(this.beacons[i0].minus(otherReoriented.beacons[j0]));
                    const nextBeacons: HashSet<XYZ> = new HashSet;
                    for (const xyz of this.beacons.concat(otherShifted.beacons)) {
                        nextBeacons.insert(xyz);
                    }
                    const nextScanners: HashSet<XYZ> = new HashSet;
                    for (const xyz of this.scanners.concat(otherShifted.scanners)) {
                        nextScanners.insert(xyz)
                    }
                    return new Scanner([...nextBeacons], [...nextScanners])
                }
            }
        }
        throw new Error("Too little intersection")
    }
}

function next<T>(ts: Iterable<T>, predicate: (t: T) => boolean): T {
    for (const t of ts) {
        if (predicate(t)) {
            return t
        }
    }
    throw new Error("No suitable value found in iteration")
}

export async function* parseInput(input: AsyncGenerator<string>) {
    let beacons = [];
    for await (const line of input) {
        if (line.includes("scanner")) {
            beacons = [];
        } else if (line.trim()) {
            const xyz = XYZ.parse(line);
            if (xyz) {
                beacons.push(xyz);
            }
        } else {
            yield new Scanner(beacons)
        }
    }
    if (beacons.length) {
        yield new Scanner(beacons)
    }
}

export function solve(scanners: Scanner[]): [number, number] {
    let mama = scanners[0].clone();
    const merged = new Set([0]);
    while (merged.size < scanners.length) {
        for (let idx = 0; idx < scanners.length; idx++) {
            if (!merged.has(idx)) {
                const s = scanners[idx];
                try {
                    mama = mama.merge(s);
                    merged.add(idx);
                } catch (err) {
                    if (err instanceof Error && err.message === "Too little intersection") {
                        // skip for now
                    } else {
                        throw err
                    }
                }
            }
        }
    }
    const part1 = mama.beacons.length;
    let part2 = 0;
    for (let idx0 = 0; idx0 < mama.scanners.length; idx0++) {
        const scanner0 = mama.scanners[idx0];
        for (let idx1 = 0; idx1 < idx0; idx1++) {
            const l = scanner0.taxicabDistance(mama.scanners[idx1]);
            part2 = Math.max(l, part2);
        }
    }
    return [part1, part2]
}

async function main(args: string[]) {
    const scanners = [];
    for await (const scanner of parseInput(readInput(...args))) {
        scanners.push(scanner);
    }
    const [part1, part2] = solve(scanners);
    console.log(`Part 1: ${part1}`);
    console.log(`Part 2: ${part2}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
