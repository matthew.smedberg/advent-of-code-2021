import Hash from "../../lib/insecureHash.ts";
import HashSet from "../../lib/hashSet.ts";
import { readInput } from "../../lib/read-input.ts";

class Pixel {
    private readonly _hash: number;

    constructor (
        readonly r: number,
        readonly c: number
    ) {
        const hash = new Hash;
        hash.ingest([r,c]);
        this._hash = hash.currentDigest();
    }

    equals(other: Pixel) {
        return this.r === other.r && this.c === other.c
    }

    hash(): number {
        return this._hash
    }
}

class Mask {
    constructor (
        private readonly m: string
    ) {}

    bit(n: number): boolean {
        return this.m.at(n) === '#'
    }
}

class Image {
    private readonly rMin: number;
    private readonly rMax: number;
    private readonly cMin: number;
    private readonly cMax: number;
    constructor (
        private readonly pixels: HashSet<Pixel>,
        private readonly frame: boolean = false,
        bounds: [number, number, number, number] | undefined = undefined,
    ) {
        switch (typeof(bounds)) {
            case 'undefined': {
                let acc: [number, number, number, number] | undefined = bounds;
                for (const pixel of pixels) {
                    const {r, c} = pixel;
                    if (acc) {
                        const [rMin, rMax, cMin, cMax] = (acc as [number, number, number, number]);
                        acc = [Math.min(r, rMin), Math.max(r, rMax), Math.min(c, cMin), Math.max(c, cMax)];
                    } else {
                        acc = [r, r, c, c];
                    }
                }
                [this.rMin, this.rMax, this.cMin, this.cMax] = acc!; break
            }
            default:
                [this.rMin, this.rMax, this.cMin, this.cMax] = bounds!;
        }
    }

    get size(): number {
        return this.pixels.size()
    }

    lit(pixel: Pixel): boolean {
        const {r, c} = pixel;
        if (this.rMin <= r && r <= this.rMax && this.cMin <= c && c <= this.cMax) {
            return this.pixels.contains(pixel)
        } else {
            return this.frame
        }
    }

    enhance(mask: Mask): Image {
        // deno-lint-ignore no-this-alias
        const self: Image = this;
        const nextBounds: [number, number, number, number] = [this.rMin - 1, this.rMax + 1, this.cMin - 1, this.cMax + 1];
        function includePixel(pixel: Pixel): boolean {
            const bits = [-1, 0, 1].flatMap(dr => [-1, 0, 1].map(dc => {
                return self.lit(new Pixel(pixel.r + dr, pixel.c + dc)) ? '1' : '0'
            })).join('');
            return mask.bit(parseInt(bits, 2))
        }

        const nextPixels = new HashSet<Pixel>();
        for (let r = nextBounds[0]; r <= nextBounds[1]; r++) {
            for (let c = nextBounds[2]; c <= nextBounds[3]; c++) {
                const pixel = new Pixel(r,c);
                if (includePixel(pixel)) {
                    nextPixels.insert(pixel);
                }
            }
        }
        const nextFrame = mask.bit((2**9 - 1) * Number(this.frame));
        return new Image(nextPixels, nextFrame, nextBounds)
    }
}

export async function parse(input: AsyncGenerator<string>): Promise<[Mask, Image]> {
    const maskLine = (await input.next()).value.trim() as string;
    const mask = new Mask(maskLine);
    const pixels: HashSet<Pixel> = new HashSet;
    let rIdx = 0;
    for await (let row of input) {
        row = row.trim();
        if (row) {
            let cIdx = 0;
            for (const s of row) {
                if (s === '#') {
                    pixels.insert(new Pixel(rIdx, cIdx))
                }
                cIdx += 1;
            }
            rIdx += 1;
        }
    }
    return [mask, new Image(pixels)]
}

export function enhance(image: Image, mask: Mask, n: number): Image {
    let e = image;
    for (let i = 0; i < n; i++) {
        e = e.enhance(mask)
    }
    return e
}

async function main(args: string[]) {
    const [mask, image0] = await parse(readInput(...args));
    const image2 = enhance(image0, mask, 2);
    console.log(`Part 1: ${image2.size}`);

    const image50 = enhance(image2, mask, 48);
    console.log(`Part 2: ${image50.size}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
