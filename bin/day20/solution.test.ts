import * as day20 from "./solution.ts";
import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

const [testMask, testImage] = await (async () => {
    const rawInput = `..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#
    
    #..#.
    #....
    ##..#
    ..#..
    ..###`;
    async function* lines() {
        yield* rawInput.split('\n')
    }
    return await day20.parse(lines())
})();

Deno.test("enhance", () => {
    const image2 = day20.enhance(testImage, testMask, 2);
    assertEquals(image2.size, 35);

    const image50 = day20.enhance(testImage, testMask, 50);
    assertEquals(image50.size, 3351);
})
