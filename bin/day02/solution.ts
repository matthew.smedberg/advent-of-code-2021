import { readInput } from "../../lib/read-input.ts";

const linePat = /(\w+)\s+(\d+)/;

export class Position1 {
    constructor(
        readonly x: number = 0,
        readonly depth: number = 0
    ) {}

    static from(obj: {x: number, depth: number}) {
        return new Position1(obj.x, obj.depth);
    }

    copy(obj: {x?: number, depth?: number}) {
        return Position1.from(Object.assign({}, this, obj));
    }

    forward(n: number): Position1 {
        return this.copy({x: this.x + n});
    }

    down(n: number): Position1 {
        return this.copy({depth: this.depth + n});
    }

    up(n: number): Position1 {
        return this.copy({depth: this.depth - n});
    }

    move(line: string): Position1 {
        const groups = line.match(linePat);
        if (groups) {
            const [_, direction, steps] = groups;
            const n = parseInt(steps, 10);
            switch (direction) {
                case "forward":
                    return this.forward(n);
                case "down":
                    return this.down(n);
                case "up":
                    return this.up(n);
                default:
                    console.warn(`Unexpected direction ${direction}`)
                    return this;
            }
        } else {
            console.warn(`Unexpected line |${line}|`)
            return this;
        }
    }
}

export class Position2 {
    constructor(
        readonly x: number = 0,
        readonly depth: number = 0,
        readonly aim: number = 0
    ) {
        this.x = x;
        this.depth = depth;
        this.aim = aim;
    }

    static from(obj: {x: number, depth: number, aim: number}) {
        return new Position2(obj.x, obj.depth, obj.aim);
    }

    copy(obj: {x?: number, depth?: number, aim?: number}) {
        return Position2.from(Object.assign({}, this, obj));
    }

    down(n: number): Position2 {
        return this.copy({aim: this.aim + n});
    }

    up(n: number): Position2 {
        return this.copy({aim: this.aim - n});
    }

    forward(n: number): Position2 {
        return this.copy({
            x: this.x + n,
            depth: this.depth + (this.aim * n)
        });
    }

    move(line: string): Position2 {
        const groups = line.match(linePat);
        if (groups) {
            const [_, direction, steps] = groups;
            const n = parseInt(steps, 10);
            switch (direction) {
                case "down":
                    return this.down(n);
                case "up":
                    return this.up(n);
                case "forward":
                    return this.forward(n);
                default:
                    console.warn(`Unexpected direction ${direction}`)
                    return this;
            }
        } else {
            console.warn(`Unparseable line: |${line}|`)
            return this
        }
    }
}

async function main(args: string[]) {
    let pos1 = new Position1();
    let pos2 = new Position2();
    for await (const line of readInput(...args)) {
        pos1 = pos1.move(line);
        pos2 = pos2.move(line);
    }
    console.log(`Final position 1: x=${pos1.x}, depth=${pos1.depth}.`)
    console.log(`Product 1: ${pos1.x * pos1.depth}`);

    console.log(`Final position 2: x=${pos2.x}, depth=${pos2.depth}, aim=${pos2.aim}`);
    console.log(`Product 2: ${pos2.x * pos2.depth}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
