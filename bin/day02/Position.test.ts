import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

import { Position1, Position2 } from "./solution.ts";

Deno.test("Position1 should follow a course", () => {
    const instructions = [
        "forward 5",
        "down 5",
        "forward 8",
        "up 3",
        "down 8",
        "forward 2",
    ];
    let pos = new Position1();
    for (const line of instructions) {
        pos = pos.move(line);
    }
    assertEquals(pos.x, 15);
    assertEquals(pos.depth, 10);
});

Deno.test("Position2 should follow a different course", () => {
    const instructions = [
        "forward 5",
        "down 5",
        "forward 8",
        "up 3",
        "down 8",
        "forward 2",
    ];
    let pos = new Position2();
    for (const line of instructions) {
        pos = pos.move(line);
    }
    assertEquals(pos.x, 15);
    assertEquals(pos.depth, 60);
});
