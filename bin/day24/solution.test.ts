import * as day24 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test("Solver: Execute instructions", () => {
    const solver = new day24.Solver;

    assertEquals(solver.z.length, 0);

    solver.execute(day24.Instruction.hold(12, 1));
    assertEquals(solver.z.length, 1);
    let { idx, offset } = solver.z[0];
    assertEquals(idx, 0);
    assertEquals(offset, 1);

    solver.execute(day24.Instruction.hold(12, 1));
    assertEquals(solver.z.length, 2);
    ({ idx, offset } = solver.z[1]);
    assertEquals(idx, 1);
    assertEquals(offset, 1);

    solver.execute(day24.Instruction.hold(15, 16));
    assertEquals(solver.z.length, 3);
    ({idx, offset} = solver.z[2]);
    assertEquals(idx, 2);
    assertEquals(offset, 16);

    solver.execute(day24.Instruction.pop(-8, 5));
    assertEquals(solver.z.length, 2);
    const [b0, b1] = solver.z;
    assertEquals(b0.idx, 0);
    assertEquals(b0.offset, 1);
    assertEquals(b1.idx, 1);
    assertEquals(b1.offset, 1);

    const [d2, d3] = solver.alu.digits.slice(2,4);
    assertEquals(d2.digit, "1");
    assertEquals(d3.value, 9);
});
