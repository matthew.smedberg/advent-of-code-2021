import { readInput } from "../../lib/read-input.ts";

enum InstructionType {
    HOLD, POP
}

export class Instruction {
    constructor(
        readonly type: InstructionType,
        readonly popOffset: number,
        readonly pushOffset: number,
    ) {}

    static hold(popOffset: number, pushOffset: number): Instruction {
        return new Instruction(InstructionType.HOLD, popOffset, pushOffset)
    }

    static pop(popOffset: number, pushOffset: number): Instruction {
        return new Instruction(InstructionType.POP, popOffset, pushOffset)
    }
}

// Represents an expression of the form d_idx + offset
class Binomial {
    constructor (
        readonly idx: number,
        readonly offset: number,
    ) {}
}

class Digit {
    private _value: number | undefined = undefined

    get value() {
        return this._value
    }

    commit(v: number) {
        if (v > 9 || v < 1 || v !== Math.floor(v)) {
            throw `Invalid digit ${v}`
        }
        switch (typeof(this.value)) {
            case "number":
                throw "Digit has already been committed!"
            case "undefined":
                this._value = v;
        }
    }

    get digit(): string {
        switch (typeof(this.value)) {
            case "undefined":
                return '?'
            default:
                return this.value.toString().slice(0, 1)
        }
    }
}

class ALU {
    readonly digits: Digit[]

    constructor() {
        this.digits = [];
        for (let i = 0; i < 14; i++) {
            this.digits.push(new Digit)
        }
    }

    commit(idx: number, value: number) {
        this.digits[idx].commit(value);
    }

    toString() {
        return this.digits.map(d => d.digit).join('')
    }
}

export class Solver {
    readonly alu = new ALU
    private _idx = 0
    get idx() {
        return this._idx
    }
    readonly z: Binomial[] = [];

    constructor(readonly maximize: boolean = true) {}

    execute(instruction: Instruction) {
        // get z % 26, i.e. the last digit pushed onto the stack
        let zLast: Binomial | undefined;
        switch (instruction.type) {
            case InstructionType.HOLD:
                zLast = this.z.at(-1); break;
            case InstructionType.POP:
                zLast = this.z.pop();
        }

        solve: {
            switch (typeof(zLast)) {
                case "undefined":
                    // Then z is empty: solve instruction.popOffset == v_idx
                    if (1 <= instruction.popOffset && instruction.popOffset <= 9) {
                        this.alu.commit(this.idx, instruction.popOffset); break solve
                    }
                    break;
                default: {
                    // solve binomial + instruction.popOffset == v_idx
                    const b = zLast.offset + instruction.popOffset;
                    if (0 <= b && b <= 8) {
                        if (this.maximize) {
                            this.alu.commit(this.idx, 9);
                            this.alu.commit(zLast.idx, 9 - b)
                        } else {
                            this.alu.commit(zLast.idx, 1);
                            this.alu.commit(this.idx, 1 + b);
                        }
                        break solve;
                    } else if (-8 <= b && b <= 0) {
                        if (this.maximize) {
                            this.alu.commit(zLast.idx, 9);
                            this.alu.commit(this.idx, 9 + b);
                        } else {
                            this.alu.commit(this.idx, 1);
                            this.alu.commit(zLast.idx, 1 - b);
                        }
                        break solve;
                    }
                }
    
            }
            // If we get here, then we could not solve the pop equation, and must push
            this.z.push(new Binomial(this.idx, instruction.pushOffset))
        }
        this._idx += 1;
    }
}

async function* instructions(input: AsyncGenerator<string>) {
    // Read 18 lines at a time from input
    // Lines 4, 5, and 15 are the only variables
    const line4Pat = /div z (\d+)/;
    const line5Pat = /add x (-?\d+)/;
    const line15Pat = /add y (-?\d+)/;
    while (true) {
        for (let i = 0; i < 4; i++) {
            await input.next();
        }
        const item4 = await input.next();
        if (item4.done) {
            break;
        }
        const line4: string = item4.value;
        const instructionType = line4Pat.exec(line4)![1] == "26" ?
            InstructionType.POP : InstructionType.HOLD;
        const line5: string = (await input.next()).value;
        const popOffset = Number(line5Pat.exec(line5)![1]);
        for (let i = 6; i < 15; i++) {
            await input.next();
        }
        const line15: string = (await input.next()).value;
        const pushOffset = Number(line15Pat.exec(line15)![1]);
        yield new Instruction(instructionType, popOffset, pushOffset);
        for (let i = 16; i < 18; i++) {
            await input.next();
        }
    }
}

async function main(args: string[]) {
    const part1 = new Solver();
    const part2 = new Solver(false);
    for await (const instr of instructions(readInput(...args))) {
        part1.execute(instr);
        part2.execute(instr);
    }
    console.log(`Part 1: ${part1.alu.toString()}`);
    console.log(`Part 2: ${part2.alu.toString()}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
