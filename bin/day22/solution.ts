import Hash from "../../lib/insecureHash.ts";
import HashSet from "../../lib/hashSet.ts";
import { readInput } from "../../lib/read-input.ts";

export class Block {
    private readonly _hash: number;
    private readonly _bound: number;

    get bound() {
        return this._bound
    }

    constructor(
        readonly xMin: number,
        readonly xMax: number,
        readonly yMin: number,
        readonly yMax: number,
        readonly zMin: number,
        readonly zMax: number,
    ) {
        if (xMin > xMax || yMin > yMax || zMin > zMax) {
            throw "Degenerate block"
        }
        const hash = new Hash;
        hash.ingest([xMin, xMax, yMin, yMax, zMin, zMax]);
        this._hash = hash.currentDigest();
        this._bound = Math.max(...[xMin, xMax, yMin, yMax, zMin, zMax].map(v => Math.abs(v)))
    }
    
    hash(): number {
        return this._hash
    }

    equals(other: Block): boolean {
        return this.xMin === other.xMin &&
            this.xMax === other.xMax &&
            this.yMin === other.yMin &&
            this.yMax === other.yMax &&
            this.zMin === other.zMin &&
            this.zMax === other.zMax
    }

    volume(): bigint {
        return BigInt(this.xMax - this.xMin + 1) * BigInt(this.yMax - this.yMin + 1) * BigInt(this.zMax - this.zMin + 1)
    }

    isDisjoint(other: Block): boolean {
        function intervalDisjoint(a0: number, a1: number, b0: number, b1: number) {
            return a1 < b0 || b1 < a0
        }

        return [
            intervalDisjoint(this.xMin, this.xMax, other.xMin, other.xMax),
            intervalDisjoint(this.yMin, this.yMax, other.yMin, other.yMax),
            intervalDisjoint(this.zMin, this.zMax, other.zMin, other.zMax),
        ].reduce((a, b) => a || b)
    }

    intersects(other: Block): boolean {
        return !this.isDisjoint(other)
    }

    *minus(other: Block) {
        function* subIntervals(a0: number, a1: number, b0: number, b1: number): Generator<[number, number, boolean]> {
            // assumes a0 <= a1 and b0 <= b1
            if (a1 < b0 || b1 < a0) {
                yield [a0, a1, false]
            } else if ( // a0 < b0 <= a1 <= b1
                a0 < b0 && b0 <= a1 && a1 <= b1
            ) {
                yield [a0, b0 - 1, false]
                yield [b0, a1, true]
            } else if ( // a0 < b0 <= b1 < a1
                a0 < b0 && b1 < a1
            ) {
                yield [a0, b0 - 1, false]
                yield [b0, b1, true]
                yield [b1 + 1, a1, false]
            } else if ( // b0 <= a0 <= b1 < a1
                b0 <= a0 && a0 <= b1 && b1 < a1
            ) {
                yield [a0, b1, true]
                yield [b1 + 1, a1, false]
            } else if (b0 <= a0 && a1 <= b1) {
                yield [a0, a1, true]
            }
        }

        for (const [xLeft, xRight, xOverlaps] of subIntervals(this.xMin, this.xMax, other.xMin, other.xMax)) {
            if (xOverlaps) {
                for (const [yLeft, yRight, yOverlaps] of subIntervals(this.yMin, this.yMax, other.yMin, other.yMax)) {
                    if (yOverlaps) {
                        for (const [zLeft, zRight, zOverlaps] of subIntervals(this.zMin, this.zMax, other.zMin, other.zMax)) {
                            if (!zOverlaps) {
                                yield new Block(xLeft, xRight, yLeft, yRight, zLeft, zRight)
                            }
                        }
                    } else {
                        yield new Block(xLeft, xRight, yLeft, yRight, this.zMin, this.zMax)
                    }
                }
            } else {
                yield new Block(xLeft, xRight, this.yMin, this.yMax, this.zMin, this.zMax)
            }
        }
    }
}

export function parse(line: string): ["on" | "off", Block] | undefined {
    const pat = /(on|off) x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)/
    const m = pat.exec(line);
    if (m) {
        const verb = m[1] as "on" | "off";
        const [xMin, xMax, yMin, yMax, zMin, zMax] = m.slice(2).map(g => parseInt(g, 10));
        return [verb, new Block(xMin, xMax, yMin, yMax, zMin, zMax)]
    }
}

function turnOn(existing: HashSet<Block>, b: Block) {
    const newBlocks = new HashSet<Block>();
    newBlocks.insert(b);

    for (const e of existing) {
        for (const n of [...newBlocks]) { // make a copy of newBlocks to avoid concurrent modification
            if (e.intersects(n)) {
                newBlocks.discard(n);
                for (const subBlock of n.minus(e)) {
                    newBlocks.insert(subBlock);
                }
            }
        }
    }

    for (const block of newBlocks) {
        existing.insert(block)
    }
}

function turnOff(existing: HashSet<Block>, b: Block) {
    function* intersectingG() {
        for (const e of existing) {
            if (e.intersects(b)) yield e
        }
    }
    const intersecting = [...intersectingG()]; // Need to strictify to avoid concurrent modification

    for (const e of intersecting) {
        existing.discard(e);
        for (const subBlock of e.minus(b)) {
            existing.insert(subBlock);
        }
    }
}

export function solve(blocks: Iterable<["on" | "off", Block]>): bigint {
    const litBlocks = new HashSet<Block>();

    for (const [verb, block] of blocks) {
        switch (verb) {
            case "on":
                turnOn(litBlocks, block); break
            case "off":
                turnOff(litBlocks, block)
        }
    }

    let r = 0n;
    for (const block of litBlocks) {
        r += block.volume();
    }
    return r
}

async function main(args: string[]) {
    const blocks: ["on" | "off", Block][] = [];
    for await (const line of readInput(...args)) {
        const result = parse(line);
        if (result) {
            blocks.push(result);
        }
    }

    function* part1Blocks() {
        for (const p of blocks) {
            if (p[1].bound <= 50) yield p
        }
    }
    const p1 = solve(part1Blocks());
    console.log(`Part 1: ${p1}`);
    const p2 = solve(blocks);
    console.log(`Part 2: ${p2}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
