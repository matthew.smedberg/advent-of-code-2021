import { assert, assertEquals } from "https://deno.land/std/testing/asserts.ts";

import * as solution from "./solution.ts";

async function* toIterator<T>(ts: T[]) {
    yield* ts
}

Deno.test("parseChallenge happy case", async () => {
    const args = ["17", "19", "17", "13", "17", ""];
    const challenge = await(solution.parseChallenge(toIterator(args)));
    assertEquals(challenge, [17, 19, 17, 13, 17])
});

Deno.test("parseChallenge sad case", async () => {
    const args = ["17", "0x17", "0xab", "ab", "foo"];
    const challenge = await(solution.parseChallenge(toIterator(args)));
    assertEquals(challenge, [17, 0, 0])
});

Deno.test("Day 01 Part 1", () => {
    const challenge = [
        199, 200, 208, 210, 200, 207, 240, 269, 260, 263
    ];
    const p = solution.part1(challenge);
    assertEquals(p, 7)
});

Deno.test("sliding should slide over an array", () => {
    const ts0 = ["foo0", "bar1", "baz2", "foo3", "bar4", "baz5"];
    let gen = solution.sliding(ts0, 2);
    assertEquals(gen.next().value, ["foo0", "bar1"]);
    assertEquals(gen.next().value, ["bar1", "baz2"]);
    assertEquals(gen.next().value, ["baz2", "foo3"]);
    assertEquals(gen.next().value, ["foo3", "bar4"]);
    const y = gen.next();
    assertEquals(y.value, ["bar4", "baz5"]);
    assert(!y.done);
    assert(gen.next().done)
    
    gen = solution.sliding(ts0, 4);
    assertEquals(gen.next().value, ["foo0", "bar1", "baz2", "foo3"]);
});

Deno.test("Day 01 Part 2", () => {
    const challenge = [
        199, 200, 208, 210, 200, 207, 240, 269, 260, 263
    ];
    const p = solution.part2(challenge);
    assertEquals(p, 5)
});
