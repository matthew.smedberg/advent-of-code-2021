import { readInput } from "../../lib/read-input.ts";
import range from "../../lib/range.ts";

type Challenge = number[];

export async function parseChallenge(input: AsyncGenerator<string>): Promise<Challenge> {
    const xs = [];
    for await (const line of input) {
        const x = parseInt(line, 10);
        if (!isNaN(x)) {
            xs.push(x);
        }
    }
    return xs
}

export function* sliding<T>(ts: T[], n: number) {
    // yield ts.slice(0, n) to ts.slice(ts.length - n, ts.length) inclusive
    for (const i of range(0, ts.length - n + 1)) {
        yield ts.slice(i, i + n);
    }
}

export function part1(xs: Challenge) {
    let n = 0;
    for (const [x0, x1] of sliding(xs, 2)) {
        if (x1 > x0) {
            n += 1;
        }
    }
    return n
}

export function part2(xs: Challenge) {
    let n = 0;
    for (const [x0, x1, x2, x3] of sliding(xs, 4)) {
        const left = x0 + x1 + x2;
        const right = x1 + x2 + x3;
        if (right > left) {
            n += 1;
        }
    }
    return n
}

async function main(args: string[]) {
    const measurements = await(parseChallenge(readInput(...args)));
    const inc1 = part1(measurements);
    console.log(`${inc1} increasting single measurments.`);

    const inc2 = part2(measurements);
    console.log(`${inc2} increasing 3-window measurements.`)
}

if (import.meta.main) {
    await main(Deno.args)
}
