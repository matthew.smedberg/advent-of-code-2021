import { readInput } from "../../lib/read-input.ts";

export class Challenge {
    constructor (
        readonly xMin: number,
        readonly xMax: number,
        readonly yMin: number,
        readonly yMax: number,
    ) {}

    static parse(line: string): Challenge | undefined {
        const pat = /x=(-?\d+)\.\.(-?\d+),\s*y=(-?\d+)\.\.(-?\d+)/
        const m = pat.exec(line);
        if (m) {
            const params = m.slice(1).map(g => parseInt(g, 10));
            return new Challenge(params[0], params[1], params[2], params[3])
        }
    }

    *trajectories(): Generator<[number, number]> {
        for (let cy = this.yMin; cy <= this.yMax; cy++) {
            for (let vy = this.yMin; vy <= -this.yMin; vy++) {
                const discriminant = Math.sqrt((2*vy + 1)**2 - 8*cy);
                if (isInt(discriminant)) {
                    const t = ((2*vy + 1) + discriminant)/2;
                    if (t > 0) {
                        for (let vx = 1; vx <= this.xMax; vx++) {
                            const rx = rX(vx, t);
                            if (this.xMin <= rx && rx <= this.xMax) {
                                yield [vx, vy]
                            }
                        }
                    }
                }
            }
        }
    }
}

function rX(v0: number, t: number): number {
    return t <= v0 ? rY(v0, t) : rY(v0, v0)
}

function rY(v0: number, t: number): number {
    return t * v0 - (t**2 - t) / 2
}

function isInt(x: number): boolean {
    return x === Math.floor(x)
}

export function part1(challenge: Challenge): number {
    let yMax = 0;
    for (const [_, vy] of challenge.trajectories()) {
        yMax = Math.max(yMax, rY(vy, vy));
    }
    return yMax
}

export function part2(challenge: Challenge): number {
    const ts = new Map<number, Set<number>>();
    for (const [vx, vy] of challenge.trajectories()) {
        if (ts.has(vx)) {
            ts.get(vx)?.add(vy);
        } else {
            ts.set(vx, new Set([vy]));
        }
    }

    let total = 0;
    for (const vxs of ts.values()) {
        total += vxs.size;
    }
    return total
}

async function main(args: string[]) {
    const challenge = Challenge.parse((await readInput(...args).next()).value!)!;
    console.log(`Part 1: ${part1(challenge)}`);
    console.log(`Part 2: ${part2(challenge)}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
