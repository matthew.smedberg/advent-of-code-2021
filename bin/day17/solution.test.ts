import * as day17 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test("Part 1", () => {
    const challenge = day17.Challenge.parse("target area: x=20..30, y=-10..-5")!;
    assertEquals(day17.part1(challenge), 45);
});

Deno.test("Part 2", () => {
    const challenge = day17.Challenge.parse("target area: x=20..30, y=-10..-5")!;
    assertEquals(day17.part2(challenge), 112);
});
