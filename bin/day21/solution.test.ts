import * as day21 from "./solution.ts";
import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

const testInput = `Player 1 starting position: 4
Player 2 starting position: 8`;

const testStarts = await (async () => {
    async function* lines() {
        yield* testInput.split('\n')
    }
    return await day21.parse(lines())
})();

Deno.test("Deterministic game", () => {
    const [start0, start1] = testStarts;
    assertEquals(day21.part1(start0, start1), 739785);
});

Deno.test("Cartesian product; 0 factors", () => {
    const product = day21.cartesian();
    assertEquals(product, [[]]);
});

Deno.test("Cartesian product; empty factor", () => {
    const product = day21.cartesian([1,2], [], [3,4]);
    assertEquals(product, []);
})

Deno.test("Cartesian product; 3 factors", () => {
    const xs = [1, 2, 3];
    const ys = ['a', 'b', 'c'];
    const zs = [100, 200];

    // deno-lint-ignore no-explicit-any
    const product = day21.cartesian<any>(xs, ys, zs);
    function* expected() {
        for (const x of xs) {
            for (const y of ys) {
                for (const z of zs) {
                    yield [x, y, z]
                }
            }
        }
    }
    assertEquals(product, [...expected()]);
});

Deno.test("Quantum game", () => {
    const [start0, start1] = testStarts;
    const [w0, w1] = day21.part2(start0, start1);
    assertEquals(w0, 444356092776315);
    assertEquals(w1, 341960390180808);
});
