import Hash from "../../lib/insecureHash.ts";
import HashMap from "../../lib/hashMap.ts";
import { readInput } from "../../lib/read-input.ts";

class DeterministicDie {
    private roller: Generator<number>;
    constructor(
        sides: number
    ) {
        function* roller() {
            let v = 0;
            while (true) {
                yield v + 1;
                v += 1;
                v %= sides
            }
        }
        this.roller = roller()
    }

    roll(): number {
        return this.roller.next().value as number
    }
}

// Part 1: Deterministic
class DiceGame {
    private readonly _score: [number, number] = [0, 0];
    get score(): [number, number] {
        return this._score.slice() as [number, number]
    }
    private readonly position: [number, number];
    private _rolls = 0;
    get rolls(): number {
        return this._rolls
    }
    private _currentPlayer = 0;
    get currentPlayer(): number {
        return this._currentPlayer
    }
    private die: DeterministicDie;

    constructor(
        start0: number,
        start1: number,
        die: number = 100,
    ) {
        this.position = [start0, start1];
        this.die = new DeterministicDie(die);
    }

    step() {
        const s = Array.of(
            this.die.roll(), this.die.roll(), this.die.roll()
        ).reduce((a, r) => a + r);
        this._rolls += 3;
        this.position[this.currentPlayer] += s;
        this.position[this.currentPlayer] %= 10;
        this._score[this.currentPlayer] += this.position[this.currentPlayer] + 1;
        this._currentPlayer += 1;
        this._currentPlayer %= 2;
    }
}

export function part1(start0: number, start1: number): number {
    const game = new DiceGame(start0, start1);
    while (Math.max(...game.score) < 1000) {
        game.step();
    }
    return Math.min(...game.score) * game.rolls
}


// Part 2
export function cartesian<T>(...arrs: T[][]): T[][] {
    return arrs.reduceRight((acc, ts) => ts.flatMap(t => acc.map(p => [t, ...p])), [[]] as T[][])
}

const rollFrequencyDistribution: Map<number, number> = (() => {
    const dist = new Map;
    const rolls = [1, 2, 3];
    for (const rs of cartesian(rolls, rolls, rolls)) {
        const s = rs.reduce((a, b) => a + b);
        const prev = dist.get(s) | 0;
        dist.set(s, prev + 1);
    }

    return dist
})();

class GameState {
    private _hash: number;

    get position(): [number, number] {
        return this.currentPosition.slice() as [number, number]
    }

    get score(): [number, number] {
        return this.currentScore.slice() as [number, number]
    }

    constructor (
        readonly currentPosition: [number, number],
        readonly currentPlayer: 0|1,
        readonly currentScore: [number, number],
    ) {
        const hash = new Hash;
        hash.ingest(currentPosition);
        hash.ingest(currentPlayer);
        hash.ingest(currentScore);
        this._hash = hash.currentDigest()
    }

    hash(): number {
        return this._hash
    }

    equals(other: GameState) {
        const [p0, p1] = this.currentPosition;
        const [p0a, p1a] = other.currentPosition;
        const [s0, s1] = this.currentScore;
        const [s0a, s1a] = other.currentScore;
        return (
            p0 === p0a && p1 === p1a && 
            this.currentPlayer === other.currentPlayer && 
            s0 === s0a && s1 === s1a
        )
    }
}

export function part2(start0: number, start1: number): [number, number] {
    const memo: HashMap<GameState, [number, number]> = new HashMap;
    
    function step(gameState: GameState, roll: number): GameState {
        const {position, score} = gameState;
        position[gameState.currentPlayer] += roll;
        position[gameState.currentPlayer] %= 10;
        score[gameState.currentPlayer] += position[gameState.currentPlayer] + 1;
        return new GameState(position, (gameState.currentPlayer + 1) % 2 as 0|1, score)
    }

    function loop(gameState: GameState): [number, number] {
        let memoizedResult = memo.get(gameState);
        switch (typeof(memoizedResult)) {
            case 'undefined':
                memoizedResult = [0,0];
                for (const [roll, freq] of rollFrequencyDistribution) {
                    const nextState = step(gameState, roll);
                    if (Math.max(...nextState.score) >= 21) {
                        memoizedResult[gameState.currentPlayer] += freq;
                    } else {
                        const conditionalWinDistribution = loop(nextState);
                        for (const idx of [0,1]) {
                            memoizedResult[idx] += freq * conditionalWinDistribution[idx];
                        }
                    }
                }
                memo.put(gameState, memoizedResult)

            /* falls through */
            default:
                return memoizedResult
        }
    }

    return loop(new GameState([start0, start1], 0, [0,0]))
}

export async function parse(input: AsyncGenerator<string>) {
    const pat = /Player \d+ starting position: (\d+)/
    const starts = [];
    for await (const line of input) {
        const m = pat.exec(line);
        if (m) {
            starts.push(parseInt(m[1], 10) - 1)
        }
    }
    return starts
}

async function main(args: string[]) {
    const [start0, start1] = await parse(readInput(...args));
    const p1 = part1(start0, start1);
    console.log(`Part 1: ${p1}`);

    const p2 = part2(start0, start1);
    console.log(`Part 2: ${Math.max(...p2)}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
