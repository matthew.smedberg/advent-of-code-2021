import * as day08 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test("Part 1: the easy letters", () => {
    let line = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
    assertEquals(day08.part1(line), 0);

    line = "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe"
    assertEquals(day08.part1(line), 2);

    line = "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc";
    assertEquals(day08.part1(line), 3);
});

Deno.test("Part 2: full parsing", () => {
    let line = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
    assertEquals(day08.part2(line), 5353);

    line = "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe"
    assertEquals(day08.part2(line), 8394);

    line = "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc";
    assertEquals(day08.part2(line), 9781);
});
