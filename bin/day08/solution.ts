import { readInput } from "../../lib/read-input.ts";

export function part1(line: string): number {
    const output = line.split('|')[1];
    return output.split(/\s+/).reduce((acc, s) => {
        switch (s.length) {
            case 2:
            case 3:
            case 4:
            case 7:
                return acc + 1
            default:
                return acc
        }
    }, 0)
}

function subsetOf(s1: string, s2: string): boolean {
    for (const c of s1) {
        if (!s2.includes(c)) {
            return false
        }
    }
    return true
}

function diff(s1: string, s2: string): string {
    const cs = [];
    for (const c of s1) {
        if (!s2.includes(c)) {
            cs.push(c);
        }
    }

    return cs.join('')
}

function sort(s: string) {
    return s.split('').sort().join('')
}

export function part2(line: string) : number {
    const digits = new Map<string, string>();
    const identified: string[] = new Array(10);
    function identify(word: string, digit: number) {
        digits.set(word, digit.toString());
        identified[digit] = word;
    }

    const [leftWords, rightWords] = line.split('|').map(s => s.trim().split(/\s+/).map(sort))

    for (const word of leftWords) {
        switch (word.length) {
            case 2:
                identify(word, 1); break
            case 3:
                identify(word, 7); break
            case 4:
                identify(word, 4); break
            case 7:
                identify(word, 8); break
        }
    }

    for (const word of leftWords) {
        if (word.length == 5 && subsetOf(identified[1], word)) {
            identify(word, 3); break
        }
    }

    for (const word of leftWords) {
        if (word.length === 5) {
            if (subsetOf(diff(identified[4], identified[3]), word)) {
                identify(word, 5);
            } else if (word !== identified[3]) {
                identify(word, 2);
            }
        } else if (word.length === 6) {
            if (subsetOf(identified[3], word)) {
                identify(word, 9);
            } else if (subsetOf(identified[1], word)) {
                identify(word, 0);
            } else {
                identify(word, 6);
            }
        }
    }

    return parseInt(rightWords.map(w => digits.get(w)).join(''), 10)
}

async function main(args: string[]) {
    let part1sum = 0;
    let part2sum = 0;
    for await (let line of readInput(...args)) {
        line = line.trim();
        if (line) {
            part1sum += part1(line);
            part2sum += part2(line);
        }
    }
    console.log(`Part 1: ${part1sum}`);
    console.log(`Part 2: ${part2sum}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
