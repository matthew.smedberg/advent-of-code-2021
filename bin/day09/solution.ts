import { readInput } from "../../lib/read-input.ts";
import zipWithIndex from "../../lib/zipWithIndex.ts";
import HashMap from "../../lib/hashMap.ts";
import HashSet from "../../lib/hashSet.ts";

type Challenge = number[][];

export async function parseChallenge(input: AsyncGenerator<string>) {
    const challenge: Challenge = [];
    const digitPat = /^\d+$/;
    for await (let line of input) {
        line = line.trim();
        if (digitPat.test(line)) {
            challenge.push(line.split('').map(d => parseInt(d, 10)));
        }
    }
    return challenge
}


export function solve(challenge: Challenge): [number, number] {
    class XY {
        constructor (
            readonly x: number,
            readonly y: number,
            readonly height: number,
        ) {}
    
        hash() {
            return `${this.x},${this.y},${this.height}`
        }
    
        equals(other: XY) {
            return this.x === other.x && this.y === other.y && this.height === other.height
        }
    }

    function* neighbors(xy: XY) {
        for (const [dx, dy] of [[1,0], [0,1], [-1,0], [0,-1]]) {
            const row = challenge[xy.x + dx];
            const h = row && row[xy.y + dy];
            switch (typeof(h)) {
                case 'number':
                    yield new XY(xy.x + dx, xy.y + dy, h)
            }
        }
    }

    function* lowPoints() {
        for (const [x, row] of zipWithIndex(challenge)) {
            for (const [y, h] of zipWithIndex(row)) {
                const xy = new XY(x, y, h)
                low: {
                    for (const nbr of neighbors(xy)) {
                        if (nbr.height <= h) { break low }
                    }
                    yield xy
                }
            }
        }
    }

    let lowPointRiskSum = 0;
    const basins = new HashMap<XY, XY>();

    for (const lowPoint of lowPoints()) {
        lowPointRiskSum += 1 + lowPoint.height;

        // do BFS from the low point
        const queue = new HashSet<XY>();
        let point: XY | undefined = lowPoint;

        while (point) {
            basins.put(point, lowPoint);
            for (const nbr of neighbors(point)) {
                if (basins.containsKey(nbr) || nbr.height === 9) {
                    // do nothing, already handled
                } else {
                    queue.insert(nbr)
                }
            }
            point = queue.pop()
        }

    }

    const basinsGrouped = new HashMap<XY, XY[]>();
    for (const [xy, lowPoint] of basins) {
        basinsGrouped.putOrUpdate(lowPoint, () => [xy], v => {
            v.push(xy);
            return v
        });
    }
    const basinSizes = [...basinsGrouped.values()].map(v => v.length)
        .sort((x,y) => x - y); // because .sort() SORTS LEXICOGRAPHICALLY!!!!!
    return [lowPointRiskSum, basinSizes.slice(-3).reduce((acc, x) => acc * x)]
}

async function main(args: string[]) {
    const challenge = await parseChallenge(readInput(...args));
    const [p1, p2] = solve(challenge);
    console.log(`Part 1: ${p1}`);

    console.log(`Part 2: ${p2}`)
}

if (import.meta.main) {
    await main(Deno.args);
}
