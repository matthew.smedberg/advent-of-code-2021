import * as day09 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

const testChallenge = await (async () => {
    async function* gen() {
        yield* `
        2199943210
        3987894921
        9856789892
        8767896789
        9899965678
        `.split('\n')
    }
    return await day09.parseChallenge(gen())
})();

Deno.test("Day 9", () => {
    const [part1, part2] = day09.solve(testChallenge);
    assertEquals(part1, 15);
    assertEquals(part2, 1134);
});
