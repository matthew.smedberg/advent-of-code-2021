import Hash from "../../lib/insecureHash.ts";
import { readInput } from "../../lib/read-input.ts";
import range from "../../lib/range.ts";
import HashSet from "../../lib/hashSet.ts";
import zipWithIndex from "../../lib/zipWithIndex.ts";

class BingoBoard {
    constructor (
        readonly cells: number[][]
    ) {}

    private static readonly whitespace = /\s+/;
    static parse(lines: string[]): BingoBoard {
        return new BingoBoard(
            lines.slice(0, 5)
                .map(line => line.trim().split(this.whitespace).map(d => parseInt(d, 10)))
        )
    }
}

class Challenge {
    constructor (
        readonly called: number[],
        readonly boards: BingoBoard[],
    ) {}
}

export async function parseChallenge(input: AsyncIterable<string>) {
    const inputIterator = input[Symbol.asyncIterator]();
    let value: string | undefined;
    ({ value } = await inputIterator.next());
    const called = value!.trim().split(",").map(i => parseInt(i, 10));
    const boards: BingoBoard[] = [];

    async function* groupInFives() {
        let done: boolean | undefined;
        while (true) {
            const a: string[] = [];
            while (a.length < 5 && !done) {
                ({value, done} = await inputIterator.next());
                if (value?.trim().length) { // filter out empty lines
                    a.push(value)
                }
            }
            if (!done) {
                yield a
            } else {
                break
            }
        }
    }
    for await (const lines of groupInFives()) {
        boards.push(BingoBoard.parse(lines))
    }

    return new Challenge(called, boards)
}

function all<T>(ts: Iterable<T>, pred: (t: T) => boolean): boolean {
    for (const t of ts) {
        if (!pred(t)) {
            return false
        }
    }
    return true
}

function any<T>(ts: Iterable<T>, pred: (t: T) => boolean): boolean {
    return !all(ts, (t) => !pred(t))
}

class XY {
    private readonly _hash: number
    constructor(
        readonly x: number,
        readonly y: number
    ) {
        const hash = new Hash;
        hash.ingest("XY");
        hash.ingest(x);
        hash.ingest(y);
        this._hash = hash.currentDigest();
    }
    
    equals(other: XY): boolean {
        return this.x === other.x && this.y === other.y
    }

    hash(): number {
        return this._hash
    }
}

class BingoGame {
    readonly marked: HashSet<XY> = new HashSet();
    constructor (
        readonly board: BingoBoard
    ) {}

    mark(call: number) {
        for (const row of range(5)) {
            for (const col of range (5)) {
                if (this.board.cells[row][col] === call) {
                    this.marked.insert(new XY(row, col));
                    return
                }
            }
        }
    }

    checkRows(): boolean {
        return any(range(5), (rIdx) => all(range(5), (cIdx) => this.marked.contains(new XY(rIdx, cIdx))))
    }

    checkColumns(): boolean {
        return any(range(5), (cIdx) => all(range(5), (rIdx) => this.marked.contains(new XY(rIdx, cIdx))))
    }

    check(): boolean {
        return this.checkRows() || this.checkColumns()
    }
}

export function part1(challenge: Challenge) {
    const games = challenge.boards.map(b => new BingoGame(b));
    function firstGame(): [number, BingoGame] | undefined {
        for (const call of challenge.called) {
            for (const game of games) {
                game.mark(call);
                if (game.check()) {
                    return [call, game]
                }
            }
        }
    }
    const [lastCall, winningGame] = firstGame()!;
    let unmarked = 0;
    for (const r of range(5)) {
        for (const c of range(5)) {
            if (!winningGame.marked.contains(new XY(r,c))) {
                unmarked += winningGame.board.cells[r][c];
            }
        }
    }
    return unmarked * lastCall
}

export function part2(challenge: Challenge) {
    const games: Map<number, BingoGame> = new Map();
    for (const [i, board] of zipWithIndex(challenge.boards)) {
        games.set(i, new BingoGame(board));
    }
    function lastGame(): [number, BingoGame] | undefined {
        for (const call of challenge.called) {
            for (const k of range(challenge.boards.length)) {
                const game = games.get(k); 
                if (typeof(game) !== 'undefined') {
                    game.mark(call);
                    if (game.check()) {
                        games.delete(k);
                        if (games.size === 0) {
                            return [call, game]
                        }
                    }
                }
            }
        }
    }
    const [lastCall, game] = lastGame()!
    let unmarked = 0;
    for (const r of range(5)) {
        for (const c of range(5)) {
            if (!game.marked.contains(new XY(r,c))) {
                unmarked += game.board.cells[r][c];
            }
        }
    }
    return unmarked * lastCall
}

async function main(args: string[]) {
    const input: AsyncGenerator<string> = readInput(...args);
    const challenge = await parseChallenge(input);
    const p1 = part1(challenge);
    console.log(`Part 1 result: ${p1}`);

    const p2 = part2(challenge);
    console.log(`Part 2 result: ${p2}`);
}

if (import.meta.main) {
    await main(Deno.args)
}
