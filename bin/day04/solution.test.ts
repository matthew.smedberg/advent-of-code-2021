import * as day04 from "./solution.ts";

import { assert, assertEquals } from "https://deno.land/std/testing/asserts.ts";

async function* testInput() {
    const fullInput = `7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

    22 13 17 11  0
     8  2 23  4 24
    21  9 14 16  7
     6 10  3 18  5
     1 12 20 15 19
    
     3 15  0  2 22
     9 18 13 17  5
    19  8  7 25 23
    20 11 10 24  4
    14 21 16 12  6
    
    14 21 17 24  4
    10 16 15  9 19
    18  8 23 26 20
    22 11 13  6  5
     2  0 12  3  7
    `;

    yield* fullInput.split('\n');    
}

Deno.test("Day 4: parseChallenge", async () => {
    const challenge = await day04.parseChallenge(testInput());

    assertEquals(challenge.called.length, 27);
    assertEquals(challenge.boards.length, 3);

    const [c0, c1] = challenge.called.slice(0, 2);
    const [c25, c26] = challenge.called.slice(-2);
    assertEquals(c0, 7);
    assertEquals(c1, 4);
    assertEquals(c25, 26);
    assertEquals(c26, 1);

    for (const c of challenge.called) {
        assert(!isNaN(c));
    }

    for (const board of challenge.boards) {
        assertEquals(board.cells.length, 5)
        for (const row of board.cells) {
            assertEquals(row.length, 5)
            for (const c of row) {
                assert(!isNaN(c))
            }
        }
    }

    const [b0, b1, b2, b3, b4] = challenge.boards[2].cells[4];
    assertEquals(b0, 2);
    assertEquals(b1, 0);
    assertEquals(b2, 12);
    assertEquals(b3, 3);
    assertEquals(b4, 7);
});

Deno.test("Day 4: part1", async () => {
    const challenge = await day04.parseChallenge(testInput());
    assertEquals(day04.part1(challenge), 4512);
});

Deno.test("Day 4: part2", async () => {
    const challenge = await day04.parseChallenge(testInput());
    assertEquals(day04.part2(challenge), 1924);
});
