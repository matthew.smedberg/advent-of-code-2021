import { createHash } from "https://deno.land/std/hash/mod.ts";

import { readInput } from "../../lib/read-input.ts";
import HashMap from "../../lib/hashMap.ts";

class XY {
    constructor(
        readonly x: number,
        readonly y: number
    ) {}

    equals(other: XY) {
        return this.x === other.x && this.y === other.y
    }

    hash(): string {
        const hash = createHash("sha1");
        hash.update(Int32Array.from([this.x, this.y]));
        return hash.toString()
    }
}

class Challenge {
    vents: [XY, XY][] = [];

    addVent(start: XY, end: XY) {
        this.vents.push([start, end]);
    }
}

export async function parseChallenge(input: AsyncIterable<string>) {
    const challenge = new Challenge();

    const pattern = /(\d+),(\d+) -> (\d+),(\d+)/
    for await (const line of input) {
        const m = line.match(pattern)
        if (m) {
            const start = new XY(parseInt(m[1], 10), parseInt(m[2], 10));
            const end = new XY(parseInt(m[3], 10), parseInt(m[4], 10));
            challenge.addVent(start, end);
        }
    }
    return challenge
}

export function overlaps(challenge: Challenge): [number, number] {
    const ventCounter = new HashMap<XY, number>();
    function increment(xy: XY) {
        ventCounter.putOrUpdate(xy, () => 1, (v) => v + 1);
    }
    for (const [start, end] of challenge.vents) {
        if (start.x === end.x) {
            for (let y = Math.min(start.y, end.y); y <= Math.max(start.y, end.y); y += 1) {
                increment(new XY(start.x, y));
            }
        } else if (start.y === end.y) {
            for (let x = Math.min(start.x, end.x); x <= Math.max(start.x, end.x); x += 1) {
                increment(new XY(x, start.y))
            }
        }
    }
    let part1 = 0;
    for (const [_, v] of ventCounter[Symbol.iterator]()) {
        if (v > 1) {
            part1 += 1;
        }
    }
    
    for (let [start, end] of challenge.vents) {
        if (start.x === end.x || start.y === end.y) {
            continue;
        }
        if (start.x > end.x) {
            [start, end] = [end, start];
        }
        const ysgn = start.y < end.y ? 1 : -1
        for (let i = 0; i <= (end.x - start.x); i += 1) {
            increment(new XY(start.x + i, start.y + ysgn*i));
        }
    }

    let part2 = 0;
    for (const [_, v] of ventCounter[Symbol.iterator]()) {
        if (v > 1) {
            part2 += 1;
        }
    }
    return [part1, part2]
}

async function main(args: string[]) {
    const challenge = await parseChallenge(readInput(...args));
    const [part1, part2] = overlaps(challenge);
    console.log(`Part 1: ${part1} overlaps found`);
    console.log(`Part 2: ${part2} overlaps found`);
}

if (import.meta.main) {
    await main(Deno.args);
}
