import { assertEquals } from "https://deno.land/std/testing/asserts.ts"

import * as day05 from "./solution.ts";

async function* fakeInput() {
    const sampleInput = `0,9 -> 5,9
    8,0 -> 0,8
    9,4 -> 3,4
    2,2 -> 2,1
    7,0 -> 7,4
    6,4 -> 2,0
    0,9 -> 2,9
    3,4 -> 1,4
    0,0 -> 8,8
    5,5 -> 8,2`;
    yield* sampleInput.split('\n')
}

Deno.test("parseChallenge should parse", async () => {
    const challenge = await day05.parseChallenge(fakeInput());
    assertEquals(challenge.vents.length, 10);

    let [{x:x0,y:y0}, {x:x1, y:y1}] = challenge.vents[0];
    assertEquals(x0, 0);
    assertEquals(y0, 9);
    assertEquals(x1, 5);
    assertEquals(y1, 9);

    [{x:x0,y:y0}, {x:x1, y:y1}] = challenge.vents[1];
    assertEquals(x0, 8);
    assertEquals(y0, 0);
    assertEquals(x1, 0);
    assertEquals(y1, 8);

    [{x:x0,y:y0}, {x:x1, y:y1}] = challenge.vents[9];
    assertEquals(x0, 5);
    assertEquals(y0, 5);
    assertEquals(x1, 8);
    assertEquals(y1, 2);
    
});

Deno.test("overlaps should compute both 4-way and 8-way", async () => {
    const challenge = await day05.parseChallenge(fakeInput());
    const [v4, v8] = day05.overlaps(challenge);
    assertEquals(v4, 5);
    assertEquals(v8, 12);
});
