import * as day25 from "./solution.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

const testInput = `
v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>
`.trim();

const testField = await (async (input) => {
    async function* gen() {
        yield* input.split('\n')
    }
    return await day25.parseChallenge(gen())
})(testInput);

Deno.test("Day 25: parse", () => {
    const {x, y} = testField.bounds;
    assertEquals(x, 10);
    assertEquals(y, 9);
})

Deno.test("Traffic jam", () => {
    const steps = day25.trafficJam(testField);
    assertEquals(steps, 58);
})

