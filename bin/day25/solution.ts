import Hash from "../../lib/insecureHash.ts";
import HashMap from "../../lib/hashMap.ts";
import { readInput } from "../../lib/read-input.ts";

class XY {
    private readonly _hash: number
    constructor(
        readonly x: number,
        readonly y: number,
    ) {
        const hash = new Hash();
        hash.ingest("XY");
        hash.ingest(x);
        hash.ingest(y);
        this._hash = hash.currentDigest();
    }

    hash(): number {
        return this._hash
    }

    equals(other: XY): boolean {
        return this.x === other.x && this.y === other.y
    }
}

enum Direction {
    SOUTH, EAST
}

function step(xy: XY, d: Direction, bounds: XY): XY {
    switch (d) {
        case Direction.EAST:
            return new XY((xy.x + 1) % bounds.x, xy.y)
        case Direction.SOUTH:
            return new XY(xy.x, (xy.y + 1) % bounds.y)
    }
}

class CucumberField {
    constructor(
        readonly field: HashMap<XY, Direction>,
        readonly bounds: XY,
    ) {}

    copy(nextField: HashMap<XY, Direction>): CucumberField {
        return new CucumberField(nextField, this.bounds)
    }
}

function stepHerd(cucumbers: CucumberField, direction: Direction): [CucumberField, number] {
    const nextField = new HashMap<XY, Direction>();
    let moveCount = 0;
    for (const [xy, d] of cucumbers.field) {
        const nextXY = step(xy, d, cucumbers.bounds);
        if (d != direction || cucumbers.field.containsKey(nextXY)) {
            nextField.put(xy, d);
        } else {
            nextField.put(nextXY, d);
            moveCount += 1;
        }
    }

    return [cucumbers.copy(nextField), moveCount]
}

export function trafficJam(startField: CucumberField): number {
    const log = import.meta.main ? 
        (msg: string) => console.log(msg) :
        (_msg: string) => {};
    
    let field = startField;
    let steps = 0;
    let moves = -1;
    while (moves) {
        const [eastField, eastMoves] = stepHerd(field, Direction.EAST);
        const [southField, southMoves] = stepHerd(eastField, Direction.SOUTH);
        moves = eastMoves + southMoves;
        steps += 1;
        log(`${steps}th step: ${moves} moves`);
        field = southField;
    }
    return steps
}

export async function parseChallenge(input: AsyncGenerator<string>): Promise<CucumberField> {
    const field = new HashMap<XY, Direction>();
    let xMax = 0;
    let y = 0;
    for await (let line of input) {
        line = line.trim();
        if (!line) {
            continue;
        }
        xMax = Math.max(xMax, line.length);
        let x = 0;
        for (const c of line) {
            switch (c) {
                case '>':
                    field.put(new XY(x,y), Direction.EAST); break;
                case 'v':
                    field.put(new XY(x,y), Direction.SOUTH); break;
            }
            x += 1;
        }
        y += 1;
    }
    return new CucumberField(field, new XY(xMax, y))
}

async function main(args: string []) {
    const field = await parseChallenge(readInput(...args));
    const moves = trafficJam(field);
    console.log(`Herd stopped moving after ${moves} moves`);
}

if (import.meta.main) {
    await main(Deno.args);
}
