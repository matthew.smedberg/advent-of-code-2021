import { readInput } from "../../lib/read-input.ts";

export enum PacketType {
    Literal, Operator
}

interface Packet {
    readonly packetType: PacketType,
    readonly version: number,
    readonly typeID: number
}

export class Literal implements Packet {
    readonly packetType = PacketType.Literal;
    readonly typeID = 4;

    constructor(
        readonly version: number,
        readonly value: number,
    ) {}
}

export class Operator implements Packet {
    readonly packetType = PacketType.Operator;

    constructor(
        readonly version: number,
        readonly typeID: number,
        readonly subPackets: Packet[],
    ) {}
}

export class BitTransmission {
    private bitIdx = 0;
    constructor (
        readonly hex: string
    ) {
        const hexPat = /^[0-9a-fA-F]*$/
        if (!hexPat.exec(hex)) {
            throw "Only hex strings are supported!"
        }
    }

    // read n bits starting at the current point position
    // the returned string will only have characters 0 and 1
    read(n: number): string {
        const currentBitIdx = this.bitIdx;
        const currentHexIdx = Math.floor(currentBitIdx / 4);
        const currentHexBit = currentBitIdx % 4;
        const endingBitIdx = currentBitIdx + n;
        if (endingBitIdx > 4*this.hex.length) {
            throw "Transmission buffer exhausted!"
        }
        const endingHexIdx = Math.floor(endingBitIdx / 4);

        const r: string[] = [];
        for (const c of this.hex.slice(currentHexIdx, endingHexIdx + 1)) {
            r.push(parseInt(c, 16).toString(2).padStart(4, '0'));
        }
        this.bitIdx = endingBitIdx;
        return r.join('').slice(currentHexBit, currentHexBit + n)
    }

    parsePacket(): Packet {
        const version = parseInt(this.read(3), 2);
        const typeID = parseInt(this.read(3), 2);
        switch (typeID) {
            case 4: {
                const qs: string[] = [];
                while (true) {
                    const quintet = this.read(5);
                    qs.push(quintet.slice(1));
                    if (quintet.at(0) === '0') {
                        break
                    }
                }
                const value = parseInt(qs.join(''), 2);
                return new Literal(version, value)
            }
            default: {
                const lengthModeID = this.read(1) as '0' | '1';
                let length: number;
                const subPackets: Packet[] = [];
                switch (lengthModeID) {
                    case '1': {
                        length = parseInt(this.read(11), 2);
                        for (let i = 0; i < length; i++) {
                            subPackets.push(this.parsePacket());
                        }
                        return new Operator(version, typeID, subPackets)
                    }
                    case '0': {
                        length = parseInt(this.read(15), 2);
                        const startBitIdx = this.bitIdx;
                        while (this.bitIdx < startBitIdx + length) {
                            subPackets.push(this.parsePacket());
                        }
                        if (this.bitIdx > startBitIdx + length) {
                            throw `Incorrect subpacket bit count!`
                        }
                        return new Operator(version, typeID, subPackets)
                    }
                }
            }
        }
    }
}

export function versionSum(packet: Packet): number {
    switch (packet.packetType) {
        case PacketType.Literal:
            return packet.version
        case PacketType.Operator:
            return (packet as Operator).subPackets.reduce(
                (acc, s) =>  acc + versionSum(s),
                packet.version
            )
    }
}

export function evaluate(packet: Packet): number {
    const operator = packet as Operator;
    switch (packet.typeID) {
        case 4:
            return (packet as Literal).value
        case 0:
            return operator.subPackets.reduce((acc, p) => acc + evaluate(p), 0)
        case 1:
            return operator.subPackets.reduce((acc, p) => acc * evaluate(p), 1);
        case 2:
            return operator.subPackets.map(evaluate).reduce((acc, v) => Math.min(acc, v))
        case 3:
            return operator.subPackets.map(evaluate).reduce((acc, v) => Math.max(acc, v))
        case 5: {
            const [left, right] = operator.subPackets.map(evaluate);
            return left > right ? 1 : 0
        }
        case 6: {
            const [left, right] = operator.subPackets.map(evaluate)
            return left < right ? 1 : 0
        }
        case 7: {
            const [left, right] = operator.subPackets.map(evaluate);
            return left === right ? 1 : 0
        }
        default:
            throw `Unsupported typeID ${packet.typeID}`
    }
}

async function main(args: string[]) {
    const line: string = (await readInput(...args).next()).value!;
    const transmission = new BitTransmission(line);
    const packet = transmission.parsePacket();
    console.log(`Part 1: ${versionSum(packet)}`);
    console.log(`Part 2: ${evaluate(packet)}`);
}

if (import.meta.main) {
    await main(Deno.args);
}
