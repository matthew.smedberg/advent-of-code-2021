import * as day16 from "./solution.ts";
import zip from "../../lib/zip.ts";

import { assertEquals } from "https://deno.land/std/testing/asserts.ts";

Deno.test("Parse literal packet", () => {
    const bt = new day16.BitTransmission("D2FE28");
    const packet = bt.parsePacket();
    assertEquals(packet.packetType, day16.PacketType.Literal);
    const literal = (packet as day16.Literal);
    assertEquals(literal.typeID, 4);
    assertEquals(literal.version, 6);
    assertEquals(literal.value, 2021);
});

Deno.test("Parse operator packet, type 0", () => {
    const bt = new day16.BitTransmission("38006F45291200");
    const packet = bt.parsePacket();
    assertEquals(packet.packetType, day16.PacketType.Operator);
    const operator = (packet as day16.Operator);
    assertEquals(operator.version, 1);
    assertEquals(operator.typeID, 6);
    assertEquals(operator.subPackets.length, 2);
    const expectedValues = [10, 20];
    for (const [expected, actual] of zip(
        expectedValues,
        (operator.subPackets as day16.Literal[]).map(p => p.value))
    ) {
        assertEquals(expected, actual);
    }
});

Deno.test("Parse operator packet, type 1", () => {
    const bt = new day16.BitTransmission("EE00D40C823060");
    const packet = bt.parsePacket() as day16.Operator;
    assertEquals(packet.packetType, day16.PacketType.Operator);
    assertEquals(packet.version, 7);
    assertEquals(packet.typeID, 3);
    assertEquals(packet.subPackets.length, 3);
    const expectedValues = [1, 2, 3];
    for (const [expected, actual] of zip(
        expectedValues,
        (packet.subPackets as day16.Literal[]).map(p => p.value),
    )) {
        assertEquals(expected, actual);
    }
});

Deno.test("Version Sum 1", () => {
    const bt = new day16.BitTransmission("8A004A801A8002F478");
    const packet0 = bt.parsePacket() as day16.Operator;
    assertEquals(packet0.version, 4);
    const [packet1] = packet0.subPackets as day16.Operator[];
    assertEquals(packet1.version, 1);
    const [packet2] = packet1.subPackets as day16.Operator[];
    assertEquals(packet2.version, 5);
    const [packet3] = packet2.subPackets;
    assertEquals(packet3.packetType, day16.PacketType.Literal);
    assertEquals(packet3.version, 6);

    assertEquals(day16.versionSum(packet0), 16);
});

Deno.test("Version Sum 2", () => {
    const bt = new day16.BitTransmission("620080001611562C8802118E34")
    const packet = bt.parsePacket() as day16.Operator;
    assertEquals(packet.version, 3);

    const [packet0, packet1] = packet.subPackets as day16.Operator[];
    assertEquals(packet0.version, 0);
    assertEquals(packet1.version, 1);

    const [packet00, packet01] = packet0.subPackets;
    assertEquals(packet00.version, 0);
    assertEquals(packet00.packetType, day16.PacketType.Literal);
    assertEquals(packet01.version, 5);
    assertEquals(packet01.packetType, day16.PacketType.Literal);

    const [packet10, packet11] = packet1.subPackets;
    assertEquals(packet10.version, 0);
    assertEquals(packet10.packetType, day16.PacketType.Literal);
    assertEquals(packet11.version, 3);
    assertEquals(packet11.packetType, day16.PacketType.Literal);

    assertEquals(day16.versionSum(packet), 12);
});

Deno.test("Version Sum 3", () => {
    const bt = new day16.BitTransmission("C0015000016115A2E0802F182340");
    const packet = bt.parsePacket() as day16.Operator;
    assertEquals(packet.version, 6);

    const [packet0, packet1] = packet.subPackets as day16.Operator[];
    assertEquals(packet0.version, 0);
    assertEquals(packet1.version, 4);

    const [packet00, packet01] = packet0.subPackets;
    assertEquals(packet00.version, 0);
    assertEquals(packet00.packetType, day16.PacketType.Literal);
    assertEquals(packet01.version, 6);
    assertEquals(packet01.packetType, day16.PacketType.Literal);

    const [packet10, packet11] = packet0.subPackets;
    assertEquals(packet10.version, 0);
    assertEquals(packet10.packetType, day16.PacketType.Literal);
    assertEquals(packet11.version, 6);
    assertEquals(packet11.packetType, day16.PacketType.Literal);

    assertEquals(day16.versionSum(packet), 23);
});

Deno.test("Evaluate", () => {
    let packet = (new day16.BitTransmission("C200B40A82")).parsePacket();
    assertEquals(day16.evaluate(packet), 3);

    packet = (new day16.BitTransmission("04005AC33890")).parsePacket();
    assertEquals(day16.evaluate(packet), 54);

    packet = (new day16.BitTransmission("880086C3E88112")).parsePacket();
    assertEquals(day16.evaluate(packet), 7);

    packet = (new day16.BitTransmission("CE00C43D881120")).parsePacket();
    assertEquals(day16.evaluate(packet), 9);

    packet = (new day16.BitTransmission("D8005AC2A8F0")).parsePacket();
    assertEquals(day16.evaluate(packet), 1);

    packet = (new day16.BitTransmission("F600BC2D8F")).parsePacket();
    assertEquals(day16.evaluate(packet), 0);

    packet = (new day16.BitTransmission("9C005AC2F8F0")).parsePacket();
    assertEquals(day16.evaluate(packet), 0);

    packet = (new day16.BitTransmission("9C0141080250320F1802104A08")).parsePacket();
    assertEquals(day16.evaluate(packet), 1);
})
