import PriorityQueue from "./priority-queue.ts";
import Hash from "../../lib/insecureHash.ts";
import HashMap from "../../lib/hashMap.ts";
import zipWithIndex from "../../lib/zipWithIndex.ts";
import { readInput } from "../../lib/read-input.ts";


type RoomNumber = 2 | 4 | 6 | 8
type HallwayNumber = 0 | 1 | 3 | 5 | 7 | 9 | 10

interface Hashable {
    equals(other: Hashable): boolean
    hash(): number
}

export class Amphipod implements Hashable {
    private readonly _hash: number
    constructor(
        readonly variant: string,
        readonly target: RoomNumber,
        readonly movementCost: bigint,
    ) {
        const hash = new Hash;
        hash.ingest("Amphipod");
        hash.ingest(variant);
        hash.ingest(target);
        hash.ingest(Number(movementCost));
        this._hash = hash.currentDigest();
    }

    equals(other: Amphipod): boolean {
        return this.variant === other.variant && this.movementCost === other.movementCost
    }

    hash(): number {
        return this._hash
    }

    get shortName() {
        return this.variant.slice(0,1).toUpperCase()
    }

    static amber  = new Amphipod("amber", 2, 1n)
    static bronze = new Amphipod("bronze", 4, 10n)
    static copper = new Amphipod("copper", 6, 100n)
    static desert = new Amphipod("desert", 8, 1000n)
}

export enum HallwayState {
    Empty,
    Full,
}

interface HallwaySquare extends Hashable {
    state: HallwayState
    pretty(): string
}

class EmptyHallway implements HallwaySquare {
    readonly state = HallwayState.Empty
    private readonly _hash: number

    constructor() {
        const hash = new Hash;
        hash.ingest("HallwaySquare");
        hash.ingest(HallwayState.Empty);
        this._hash = hash.currentDigest();
    }

    equals(other: HallwaySquare): boolean {
        return other.state === HallwayState.Empty
    }

    hash(): number {
        return this._hash
    }

    pretty(): string {
        return "#_#"
    }
}

const emptyHallway = new EmptyHallway();

export class FullHallway implements HallwaySquare {
    readonly state = HallwayState.Full
    private readonly _hash: number

    constructor(
        readonly occupant: Amphipod
    ) {
        const hash = new Hash;
        hash.ingest("HallwaySquare");
        hash.ingest(HallwayState.Full);
        hash.ingest(occupant.hash());
        this._hash = hash.currentDigest();
    }
    
    equals(other: HallwaySquare): boolean {
        switch (other.state) {
            case HallwayState.Full: {
                const h = other as unknown as FullHallway;
                return this.occupant.equals(h.occupant);
            }
            default:
                return false
        }
    }

    hash(): number {
        return this._hash
    }

    pretty(): string {
        return `#${this.occupant.shortName}#`
    }
}

export class SideRoom implements Hashable {
    private readonly _hash: number
    readonly occupants: Amphipod[]

    constructor(
        readonly capacity: number,
        ...occupants: Amphipod[] // from "bottom" to "top" 
    ) {
        if (occupants.length > capacity) {
            throw "No more capacity!"
        }
        this.occupants = occupants;
        const hash = new Hash;
        hash.ingest("SideRoom");
        hash.ingest(capacity);
        hash.ingest(occupants.map(x => x.hash()));
        this._hash = hash.currentDigest();
    }

    equals(other: SideRoom): boolean {
        if (this.capacity !== other.capacity || this.occupants.length !== other.occupants.length) {
            return false
        }
        for (let i = 0; i < this.occupants.length; i++) {
            if (!this.occupants[i].equals(other.occupants[i])) {
                return false
            }
        }
        return true
    }

    hash(): number {
        return this._hash
    }

    pretty(): string {
        const shortNames = this.occupants.map(a => a.shortName);
        while (shortNames.length < this.capacity) {
            shortNames.push('_')
        }
        return `#_${shortNames.reverse().join('')}#`
    }

    get nonEmpty() {
        return !!this.occupants.length
    }

    get size() {
        return this.occupants.length
    }

    get freeCapacity() {
        return this.capacity - this.occupants.length
    }

    admit(amphipod: Amphipod): SideRoom {
        const nextOccupants = this.occupants.slice();
        nextOccupants.push(amphipod);
        return new SideRoom(
            this.capacity,
            ...nextOccupants
        )
    }

    expel(): [Amphipod, SideRoom] {
        const nextOccupants = this.occupants.slice();
        const amphipod = nextOccupants.pop();
        switch (typeof(amphipod)) {
            case "undefined":
                throw "Room is empty"
            default:
                return [
                    amphipod,
                    new SideRoom(
                        this.capacity,
                        ...nextOccupants
                    )
                ]
        }
    }
}

type HallwayParams = {
    h0: HallwaySquare | undefined,
    h1: HallwaySquare | undefined,
    h3: HallwaySquare | undefined,
    h5: HallwaySquare | undefined,
    h7: HallwaySquare | undefined,
    h9: HallwaySquare | undefined,
    h10: HallwaySquare | undefined,
}
type BurrowState = [HallwaySquare, HallwaySquare, SideRoom, HallwaySquare, SideRoom, HallwaySquare, SideRoom, HallwaySquare, SideRoom, HallwaySquare, HallwaySquare];
export class Burrow implements Hashable {
    private readonly _hash: number
    private readonly state: BurrowState

    constructor (
        s2: SideRoom,
        s4: SideRoom,
        s6: SideRoom,
        s8: SideRoom,
        hallways: HallwayParams = {} as HallwayParams,
    ) {
        const {
            h0 = emptyHallway,
            h1 = emptyHallway,
            h3 = emptyHallway,
            h5 = emptyHallway,
            h7 = emptyHallway,
            h9 = emptyHallway,
            h10 = emptyHallway,
        } = hallways;
        this.state = [h0, h1, s2, h3, s4, h5, s6, h7, s8, h9, h10];
        const hash = new Hash;
        hash.ingest("Burrow");
        for (const v of this.state) {
            hash.ingest(v.hash())
        }
        this._hash = hash.currentDigest()
    }

    static fromState(state: BurrowState): Burrow {
        const [h0, h1, s2, h3, s4, h5, s6, h7, s8, h9, h10] = state;
        const hallways = {
            h0, h1, h3, h5, h7, h9, h10
        };
        return new Burrow(s2, s4, s6, s8, hallways)
    }

    pretty(): string {
        return this.state.map(r => r.pretty()).join('\n')
    }

    get isOrganized(): boolean {
        for (let x = 0; x <= 10; x++) {
            switch (x) {
                case 2:
                case 4:
                case 6:
                case 8: {
                    const room = this.sideRoom(x);
                    for (const a of room.occupants) {
                        if (a.target !== x) {
                            return false
                        }
                    }
                    break;
                }
                default: {
                    const hallway = this.hallwaySquare(x as HallwayNumber);
                    if (hallway.state === HallwayState.Full) {
                        return false
                    }
                }
            }
        }
        return true
    }

    hallwaySquare(idx: HallwayNumber): HallwaySquare {
        return this.state[idx]
    }

    sideRoom(idx: RoomNumber): SideRoom {
        return this.state[idx]
    }

    *legalMoves(): Generator<[Burrow, bigint]> {
        srcLoop:
        for (let src = 0; src <= 10; src++) {
            switch (src) {
                case 0:
                case 1:
                case 3:
                case 5:
                case 7:
                case 9:
                case 10: {
                    // If starting in the hallway, the only move we could make is move to our destination room
                    const srcHallway = this.hallwaySquare(src as HallwayNumber);
                    if (srcHallway instanceof FullHallway) {
                        const { occupant } = srcHallway;
                        const { target } = occupant;
                        
                        // First, check if the target room has been cleared of squatters
                        const targetRoom = this.sideRoom(target);
                        switch (targetRoom.freeCapacity) {
                            case 0: continue srcLoop;
                            default:
                                for (const a of targetRoom.occupants) {
                                    if (a.target !== target) {
                                        continue srcLoop;
                                    }
                                }
                            }

                        // Now check that the hallway is clear
                        const step = target > src ? 1 : -1;
                        const test = target > src ? (x: number) => x < target : (x: number) => x > target;
                        hallwayLoop: {
                            for (let x = src + step; test(x); x += step) {
                                switch (x) {
                                    case 0:
                                    case 1:
                                    case 3:
                                    case 5:
                                    case 7:
                                    case 9:
                                    case 10: {
                                        const h = this.hallwaySquare(x as HallwayNumber);
                                        if (h.state === HallwayState.Full) {
                                            break hallwayLoop;
                                        }
                                    }
                                }
                            }
                            // If the loop is not broken, then the hallway is clear
                            const nextState = this.state.slice() as BurrowState;
                            nextState[src] = emptyHallway;
                            nextState[target] = targetRoom.admit(srcHallway.occupant);
                            const nextBurrow = Burrow.fromState(nextState);
                            const stepCost = BigInt(Math.abs(target - src) + targetRoom.freeCapacity) * srcHallway.occupant.movementCost;
                            yield [nextBurrow, stepCost];
                            continue srcLoop
                        }
                    } else {
                        continue srcLoop
                    }
                    break
                }
                default: {
                    const srcRoom: SideRoom = this.sideRoom(src as RoomNumber);
                    if (srcRoom.size === 0) {
                        continue srcLoop;
                    }
                    // If there are no squatters, don't move
                    squatterLoop: {
                        for (const occupant of srcRoom.occupants) {
                            if (occupant.target !== src) {
                                break squatterLoop
                            }
                        }
                        continue srcLoop
                    }
                    const [a, nextSrcRoom] = srcRoom.expel();
                    directionLoop:
                    for (const step of [-1, 1]) {
                        xLoop:
                        for (let x = src + step; 0 <= x && x <= 10; x += step) {
                            switch (x) {
                                case 0:
                                case 1:
                                case 3:
                                case 5:
                                case 7:
                                case 9:
                                case 10: {
                                    const h = this.hallwaySquare(x as HallwayNumber);
                                    if (h.state === HallwayState.Full) {
                                        continue directionLoop;
                                    }
                                    const nextState = this.state.slice() as BurrowState;
                                    nextState[src] = nextSrcRoom;
                                    nextState[x] = new FullHallway(a);
                                    const nextBurrow = Burrow.fromState(nextState);
                                    const stepCost = BigInt(nextSrcRoom.freeCapacity + Math.abs(x - src)) * a.movementCost;
                                    yield [nextBurrow, stepCost];
                                    continue xLoop
                                }
                                default: {
                                    if (x !== a.target) {
                                        continue xLoop
                                    }
                                    const destRoom = this.sideRoom(x as RoomNumber);
                                    if (destRoom.freeCapacity === 0) {
                                        continue xLoop
                                    }
                                    squatterCheck: {
                                        for (const occupant of destRoom.occupants) {
                                            if (occupant.target !== x) {
                                                break squatterCheck
                                            }
                                        }
                                        const nextDestRoom = destRoom.admit(a);
                                        const nextState = this.state.slice() as BurrowState;
                                        nextState[src] = nextSrcRoom;
                                        nextState[x] = nextDestRoom;
                                        const nextBurrow = Burrow.fromState(nextState);
                                        const stepCost = BigInt(nextSrcRoom.freeCapacity + Math.abs(x - src) + destRoom.freeCapacity) * a.movementCost;
                                        yield [nextBurrow, stepCost]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    equals(other: Burrow): boolean {
        for (let i = 0; i <= 10; i++) {
            // since we know that each Burrow's .state has compatible factors, this is safe
            if (!this.state[i].equals(other.state[i] as SideRoom)) {
                return false
            }
        }
        return true
    }

    hash(): number {
        return this._hash
    }
}

export function dijkstra(initial: Burrow): [Burrow, bigint] {
    const queue = new PriorityQueue<Burrow>();
    const visited = new HashMap<Burrow, bigint>();
    queue.push(initial, 0n);
    visited.put(initial, 0n);

    while (queue.nonEmpty) {
        const [burrow, cost] = queue.pop()!;
        if (burrow.isOrganized) {
            return [burrow, cost]
        }
        const bestCost = visited.get(burrow)!;
        if (cost !== bestCost) {
            continue
        }
        for (const [neighbor, stepCost] of burrow.legalMoves()) {
            const pathCost = cost + stepCost;
            const neighborBestCost = visited.get(neighbor);
            if (typeof(neighborBestCost) === "undefined" || pathCost < neighborBestCost) {
                visited.put(neighbor, pathCost);
                queue.push(neighbor, pathCost);
            }
        }
    }
    throw "Search exhausted"
}

export async function parsePuzzle(input: AsyncIterable<string>): Promise<Burrow> {
    const occupants: Amphipod[][] = [[], [], [], []];
    for await (const line of input) {
        lineLoop:
        for (const [idx, c] of zipWithIndex(line)) {
            let room: Amphipod[];
            switch (idx) {
                case 3: room = occupants[0]; break;
                case 5: room = occupants[1]; break;
                case 7: room = occupants[2]; break;
                case 9: room = occupants[3]; break;
                default: continue lineLoop;
            }
            let amphipod: Amphipod;
            switch (c) {
                case 'A': amphipod = Amphipod.amber; break;
                case 'B': amphipod = Amphipod.bronze; break;
                case 'C': amphipod = Amphipod.copper; break;
                case 'D': amphipod = Amphipod.desert; break;
                default: continue lineLoop;
            }
            room.push(amphipod);
        }
    }
    const room2 = new SideRoom(2, ...occupants[0].reverse());
    const room4 = new SideRoom(2, ...occupants[1].reverse());
    const room6 = new SideRoom(2, ...occupants[2].reverse());
    const room8 = new SideRoom(2, ...occupants[3].reverse());
    return new Burrow(room2, room4, room6, room8)
}

export function unfold(burrow: Burrow): Burrow {
    const occupants2 = burrow.sideRoom(2).occupants.slice();
    occupants2.splice(1, 0, Amphipod.desert, Amphipod.desert);

    const occupants4 = burrow.sideRoom(4).occupants.slice();
    occupants4.splice(1, 0, Amphipod.bronze, Amphipod.copper);

    const occupants6 = burrow.sideRoom(6).occupants.slice();
    occupants6.splice(1, 0, Amphipod.amber, Amphipod.bronze);
    
    const occupants8 = burrow.sideRoom(8).occupants.slice();
    occupants8.splice(1, 0, Amphipod.copper, Amphipod.amber);

    return new Burrow(
        new SideRoom(4, ...occupants2),
        new SideRoom(4, ...occupants4),
        new SideRoom(4, ...occupants6),
        new SideRoom(4, ...occupants8),
    )
}

async function main(args: string[]) {
    const burrow2 = await parsePuzzle(readInput(...args));
    const burrow4 = unfold(burrow2);

    const [_finalBurrow2, cost2] = dijkstra(burrow2);
    console.log(`Part 1: ${cost2}`);

    const [_finalBurrow4, cost4] = dijkstra(burrow4);
    console.log(`Part 2: ${cost4}`);
}

if (import.meta.main) {
    main(Deno.args);
}
