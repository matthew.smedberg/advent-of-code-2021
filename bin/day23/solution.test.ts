import * as day23 from './solution.ts';

import { assert, assertEquals, fail } from 'https://deno.land/std/testing/asserts.ts';

const testInput = `
#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########
`;
const testBurrow = await (async () => {
    async function* lines() {
        yield* testInput.split('\n')
    }
    return await day23.parsePuzzle(lines())
})()

Deno.test("Parse puzzle", () => {
    assertEquals(testBurrow.sideRoom(2).occupants, [day23.Amphipod.amber, day23.Amphipod.bronze]);
    assertEquals(testBurrow.sideRoom(4).occupants, [day23.Amphipod.desert, day23.Amphipod.copper]);
    assertEquals(testBurrow.sideRoom(6).occupants, [day23.Amphipod.copper, day23.Amphipod.bronze]);
    assertEquals(testBurrow.sideRoom(8).occupants, [day23.Amphipod.amber, day23.Amphipod.desert]);

    for (const x of [0, 1, 3, 5, 7, 9, 10]) {
        assertEquals(testBurrow.hallwaySquare(x as 0).state, day23.HallwayState.Empty);
    }
});

Deno.test("Unfold puzzle", () => {
    const {amber, bronze, copper, desert} = day23.Amphipod;
    const burrow4 = day23.unfold(testBurrow);
    assertEquals(
        burrow4.sideRoom(2).occupants,
        [amber, desert, desert, bronze],
    );
    assertEquals(
        burrow4.sideRoom(4).occupants,
        [desert, bronze, copper, copper],
    );
    assertEquals(
        burrow4.sideRoom(6).occupants,
        [copper, amber, bronze, bronze],
    );
    assertEquals(
        burrow4.sideRoom(8).occupants,
        [amber, copper, amber, desert]
    );
})

Deno.test("Burrow: legal moves", () => {
    let current = testBurrow;

    function assertNeighbors(burrow: day23.Burrow, expected: number) {
        let c = 0;
        for (const _nbr of burrow.legalMoves()) {
            c += 1;
        }
        assertEquals(c, expected)
    }
    function findNextBurrow(
        burrow: day23.Burrow,
        expectedCost: bigint,
        test: (b: day23.Burrow) => boolean
    ): day23.Burrow {
        for (const [b, c] of burrow.legalMoves()) {
            if (c === expectedCost && test(b)) {
                return b;
            }
        }
        fail("Expected burrow not found!")
    }

    assertNeighbors(current, 28);
    current = findNextBurrow(current, 40n, (burrow) => {
        const room6 = burrow.sideRoom(6);
        const h3 = burrow.hallwaySquare(3);
        return room6.freeCapacity === 1 &&
            h3.state === day23.HallwayState.Full &&
            (h3 as day23.FullHallway).occupant.shortName === 'B'
    });
    assertNeighbors(current, 11);
    assert(!current.isOrganized);

    current = findNextBurrow(current, 400n, (burrow) => {
        const room4 = burrow.sideRoom(4);
        const room6 = burrow.sideRoom(6);
        return room4.freeCapacity === 1 && room6.freeCapacity === 0
    });
    assertNeighbors(current, 10);
    assert(!current.isOrganized);

    current = findNextBurrow(current, 3000n, (burrow) => {
        const room4 = burrow.sideRoom(4);
        const h5 = burrow.hallwaySquare(5);
        return room4.freeCapacity === 2 &&
            h5.state === day23.HallwayState.Full &&
            (h5 as day23.FullHallway).occupant.shortName === 'D'
    });
    assertNeighbors(current, 6);
    assert(!current.isOrganized);

    current = findNextBurrow(current, 30n, (burrow) => {
        const h3 = burrow.hallwaySquare(3);
        const room4 = burrow.sideRoom(4);
        return h3.state === day23.HallwayState.Empty &&
            room4.freeCapacity === 1
    });
    assertNeighbors(current, 7);
    assert(!current.isOrganized);
    
    current = findNextBurrow(current, 40n, (burrow) => {
        const room2 = burrow.sideRoom(2);
        const room4 = burrow.sideRoom(4);
        return room2.freeCapacity === 1 &&
            room4.freeCapacity === 0
    });
    assertNeighbors(current, 3);
    assert(!current.isOrganized);

    current = findNextBurrow(current, 2000n, (burrow) => {
        const room8 = burrow.sideRoom(8);
        const h7 = burrow.hallwaySquare(7);
        return room8.freeCapacity === 1 &&
            h7.state === day23.HallwayState.Full &&
            (h7 as day23.FullHallway).occupant.shortName === 'D'
    });
    assertNeighbors(current, 2);
    assert(!current.isOrganized);

    current = findNextBurrow(current, 3n, (burrow) => {
        const room8 = burrow.sideRoom(8);
        const h9 = burrow.hallwaySquare(9);
        return room8.freeCapacity === 2 &&
            h9.state === day23.HallwayState.Full &&
            (h9 as day23.FullHallway).occupant.shortName === 'A'
    });
    assert(!current.isOrganized);

    // The remainder of the states only have one legal move
    let cost;
    [[current, cost]] = [...current.legalMoves()];
    assertEquals(cost, 3000n);
    assertEquals(current.hallwaySquare(7).state, day23.HallwayState.Empty);
    assertEquals(current.sideRoom(8).freeCapacity, 1);
    assert(!current.isOrganized);

    [[current, cost]] = [...current.legalMoves()];
    assertEquals(cost, 4000n);
    assertEquals(current.hallwaySquare(5).state, day23.HallwayState.Empty);
    assertEquals(current.sideRoom(8).freeCapacity, 0);
    assert(!current.isOrganized);

    [[current, cost]] = [...current.legalMoves()];
    assertEquals(cost, 8n);
    assertEquals(current.hallwaySquare(9).state, day23.HallwayState.Empty);
    assertEquals(current.sideRoom(2).freeCapacity, 0);
    assert(current.isOrganized);
});

Deno.test("Dijkstra part 1", () => {
    const [_b, cost] = day23.dijkstra(testBurrow);
    assertEquals(cost, 12521n);
});

Deno.test("Dijkstra part 2", () => {
    const [_b, cost] = day23.dijkstra(day23.unfold(testBurrow));
    assertEquals(cost, 44169n);
})
