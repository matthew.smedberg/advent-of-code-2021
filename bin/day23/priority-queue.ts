class QueueItem<T> {
    constructor(
        readonly priority: bigint,
        readonly item: T,
    ) {}
}
export default class PriorityQueue<T> {
    private readonly underlying: QueueItem<T>[] = []

    // Heap invariant:
    // underlying[0], if it exists, is the min element

    // underlying[k] <= underlying[2k + 1]
    // underlying[k] <= underlying[2k + 2]
    // if these exist (these are the "children" of node k)

    private swap(idx0: number, idx1: number) {
        [this.underlying[idx0], this.underlying[idx1]] = [this.underlying[idx1], this.underlying[idx0]]
    }

    private bubbleUp(idx: number) {
        if (idx === 0) return;
        const item = this.underlying[idx]!;
        const parentIdx = (idx % 2 === 0) ? (idx - 2)/2 : (idx - 1)/2;
        const parent = this.underlying[parentIdx];
        if (item.priority < parent.priority) {
            this.swap(idx, parentIdx);
            this.bubbleUp(parentIdx);
        }
    }

    private bubbleDown(idx: number) {
        const item = this.underlying[idx];
        const [leftChildIdx, rightChildIdx] = [2*idx + 1, 2*idx + 2];
        if (rightChildIdx < this.underlying.length) {
            const [leftChild, rightChild] = [this.underlying[leftChildIdx], this.underlying[rightChildIdx]];
            if (leftChild.priority <= rightChild.priority && leftChild.priority < item.priority) {
                this.swap(idx, leftChildIdx);
                this.bubbleDown(leftChildIdx);
            } else if (rightChild.priority <= leftChild.priority && rightChild.priority < item.priority) {
                this.swap(idx, rightChildIdx);
                this.bubbleDown(rightChildIdx);
            }
        } else if (leftChildIdx < this.underlying.length) {
            const leftChild = this.underlying[leftChildIdx];
            if (item.priority > leftChild.priority) {
                this.swap(idx, leftChildIdx);
                // No need to recurse since leftChild is a leaf
            }
        }
    }

    push(item: T, priority: bigint) {
        const idx = this.underlying.push(new QueueItem(priority, item)) - 1;
        this.bubbleUp(idx);
    }

    pop(): [T, bigint] | undefined {
        if (this.underlying.length === 0) {
            return
        }
        this.swap(0, this.underlying.length - 1);
        const retItem = this.underlying.pop()!;
        this.bubbleDown(0);

        return [retItem.item, retItem.priority]
    }

    get isEmpty(): boolean {
        return this.underlying.length === 0
    }

    get nonEmpty(): boolean {
        return !this.isEmpty
    }
}
