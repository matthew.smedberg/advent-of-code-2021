FROM gitpod/workspace-full

ENV DENO_INSTALL=/deno
RUN sudo mkdir -p /deno &&\
    sudo chown -R gitpod /deno &&\
    curl -fsSL https://deno.land/x/install/install.sh | sh

ENV PATH=${DENO_INSTALL}/bin:${PATH} \
    DENO_DIR=${DENO_INSTALL}/.cache/deno
