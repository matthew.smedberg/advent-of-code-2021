# registry.gitlab.com/matthew.smedberg/advent-of-code-2021/ci

FROM denoland/deno:ubuntu

RUN apt-get update &&\
    apt-get install -y make git &&\
    apt-get clean &&\
    rm -rf /var/lib/apt/lists/*
