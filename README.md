# advent-of-code-2021

Solutions to Advent Of Code 2021 coding challenges, using TypeScript. All
scripts expect to be run using the [Deno](https://deno.land) runtime.

Each solution script expects to either be given the filename of a challenge file,
or for the challenge to be piped in via standard input. In the former case, of course,
you will need to use the `--allow-read` flag to give Deno permission to read that file.
